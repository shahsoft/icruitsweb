Welcome To The Freedom's Gate 3D Creation (FG3D)


********************************************
************** GETTING STARTED *************
********************************************
it's all very easy

1) Open the demo scene that you will find inside the package

2) Select the 3D character

3) In the Inspector, locate the "FG3D Customizer" script

4) Select the object to import into 3D character and follow the instructions.

5) Whenever you import a object, the "add Item" slot is reset and so you can import a new object.

6) The added tools (rotate object in edit mode and set render scene), allow you a better understanding of-skinned meshes you go to add.

7) At the end you can save your 3D character like Prefab. You can find this "Custom Prefab" in this directory: Assets/Prefab/FG3D Customizer"

8) Press play and try the animation

9) Video link useful to better understand how FG3d Customizer works: https://www.youtube.com/watch?v=XdYPHHsZPrk&feature=youtu.be

10) How to Export cloth and hair from Daz3d Studio to Unity? Tutorial on my website https://fg3d.net/en/

Remember: this is a method that uses shared meshes, like a  animated human mesh and a piece of clothing that is rigged with the same bones. Basically the skinned-mesh object and your 3D character, must have the same skeleton.

MauSan

********************************************
************** VERSION HISTORY *************
********************************************
1.2

Changed GUI
Added Rotate object in edit mode
Added Set Render State

**************

1.0 (First release)


********************************************
****************** CREDITS *****************
********************************************

