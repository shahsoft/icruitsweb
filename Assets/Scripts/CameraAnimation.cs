﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimation : MonoBehaviour {


	// Use this for initialization
	void Start () {
		
	}

	/// <summary>
	/// Raises the animation complete event.
	/// </summary>
	public void OnAnimationComplete()
	{
		GetComponentInChildren<VoiceHandler>().iScript.isInterationCompleted = true;
	}
}
