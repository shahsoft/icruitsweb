﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.shephertz.app42.paas.sdk.csharp;
using com.shephertz.app42.paas.sdk.csharp.upload;
using System;
using System.IO;

public class AudioUploader : MonoBehaviour
{
    public static int uploadCount = 0;
    public Transform loadImage;

    void Start()
    {
#if !UNITY_WEBGL
        App42API.Initialize("af42583ec4198833cf178138a0916a8348bd17297744a4c0d0c1f4c517a846e3", "78e8e73c9ebf33e20eef18ef6b9a41971cc081b67ea89f290f6a779eb450d2a6");

        StartCoroutine(UploadRecordings());
#else
        Application.OpenURL("about:blank");
#endif
    }

    IEnumerator UploadRecordings()
    {
        string userName = SystemInfo.deviceName;
        string description = "Recorded Audio";
        string fileType = UploadFileType.AUDIO;
        string session = DateTime.UtcNow.ToString(@"yyyy-MM-dd") + " " + DateTime.UtcNow.ToLongTimeString() + " ";
        App42Log.SetDebug(true);        //Prints output in your editor console  

        UploadService uploadService = App42API.BuildUploadService();

        var path = Path.Combine(Application.persistentDataPath + "/Audio/");
        var info = new DirectoryInfo(path);
        var fileInfo = info.GetFiles();
        foreach (var file in fileInfo)
        {
            //if (uploadCount < 3)
            //{//
                uploadService.UploadFileForUser(session + file.Name, userName, Path.Combine(path, file.Name), fileType, description, new UnityCallBack());
                uploadCount++;
           // }
           // else
                yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(1f);
        while (uploadCount > 0)
            yield return new WaitForSeconds(0.1f);
        Application.Quit();
    }
    
    // Update is called once per frame
    void Update()
    {
        loadImage.Rotate(0, 0, -180 * Time.deltaTime);
    }
}

public class UnityCallBack : App42CallBack
{
    public void OnSuccess(object response)
    {
        if (AudioUploader.uploadCount>0) AudioUploader.uploadCount--;
        Upload upload = (Upload)response;
        IList<Upload.File> fileList = upload.GetFileList();
        for (int i = 0; i < fileList.Count; i++)
        {
            App42Log.Console("fileName is " + fileList[i].GetName());
            App42Log.Console("fileType is " + fileList[i].GetType());
            App42Log.Console("fileUrl is " + fileList[i].GetUrl());
            App42Log.Console("TinyUrl Is  : " + fileList[i].GetTinyUrl());
            App42Log.Console("fileDescription is " + fileList[i].GetDescription());
        }
    }

    public void OnException(Exception e)
    {
        if (AudioUploader.uploadCount > 0) AudioUploader.uploadCount--;
        App42Log.Console("Exception : " + e);
    }
}
