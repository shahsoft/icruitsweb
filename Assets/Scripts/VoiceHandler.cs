﻿using FrostweepGames.SpeechRecognition.Google.Cloud;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using VRStandardAssets.Utils;
using BestHTTP.ServerSentEvents;
using BestHTTP.WebSocket;
using System.IO;
using System.Net.Sockets;
using System.Text;
using IBM.Watson.DeveloperCloud.Logging;
using IBM.Watson.DeveloperCloud.Services.SpeechToText.v1;
using IBM.Watson.DeveloperCloud.Utilities;
using IBM.Watson.DeveloperCloud.DataTypes;
using IBM.Watson.DeveloperCloud.Widgets;
using IBM.Watson.DeveloperCloud.Connection;
using SimpleJSON;
using UnityEngine.Video;

public class VoiceHandler : MonoBehaviour {

    //public SpeechRecognitionModule googleSpeech;
	private ILowLevelSpeechRecognition _speechRecognition;

	public Button _startRecordButton,
	_stopRecordButton,
	_startRuntimeDetection,
	_stopRuntimeDetection;

	public SelectionSlider startRecordSlider;
	public SelectionSlider stopRecordSlider;

	public Toggle _isRuntimeDetectionToggle;

	public Image _speechRecognitionState;

	public Text _speechRecognitionResult;

	public Text inRoomTestData;

	public InputField _contextPhrases;

	public AudioClip sampleClip;

	public VideoModelScript vModelScript;
	public VideoModelScript v2ModelScript;
	public VideoModelScript v3ModelScript;
    public static string testText= "";
	private string keyString;
	private string currentClip = "idleClip";
	private string jobProfile = "final_demo";
	private string currentSessionId = "1";
	public Text messageEvent;
	public Text sessionDebugText;
	private EventSource sse;
	//	private SocketManager sManager;
	//	private Socket mainSocket;

	//Socket Varaibles
	//private static byte[] recievebuffer = new byte[1024];

	//private static int RecieveBufferSize = 0;
	//private static int SendBufferSize = 0;
	////private static Queue<string> RecieveQueue = new Queue<string>();
	////private static Queue<string> SendQueue = new Queue<string>();
	//private static readonly object SendQueueLocker = new object();
	//private static readonly object RecieveQueueLocker = new object();
	//private static readonly object SendBufferLocker = new object();
	//private static readonly object RecieveBufferLocker = new object();
	//private static readonly object SendThreadLocker = new object();
	//private static readonly object RecieveThreadLocker = new object();
	//private static bool RecieveThreadFree = true;
	//private static bool SendThreadFree = true;
	//private static bool RecieveQueueInUse = false;
	//private static bool SendQueueInUse = false;
	//private static bool wasPaused = false;
	//private bool socketReady = false;

	//private static Socket mySocket = new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);

	private WebSocket webSocket;
	// End of socket variables
	public InteractionScript iScript;
	public UIHandler uiHandler;

	private bool isSessionEnded = false;
	private bool isRecord = false;
	private string _microphoneDevice;
	private AudioClip _micRecordClip;
	private AudioClip m_AudioClip;
	private AudioData m_AudioData;
	private float[] _checkingSamples;
	private List<float> _workingData = new List<float>();
	private int lastSample;
	private bool isInitialVideo = true;
	private bool isInitialVideo2 = true;
	private string continousVideoClip;
	private string continousVideoClip2;
    private bool isContinousVideo;
	private bool isContinousVideo2;

    private string resulttextForConvApi;

	private bool isListen = false;
	private bool isSecurityCleared = false;
	private bool isEndClip = false;
	private bool isPauseClip = false;

    private int m_RecordingRoutine = 0;
	private string m_MicrophoneID = null;
	private AudioClip m_Recording = null;
	private int m_RecordingBufferSize = 2;
	private int m_RecordingHZ = 22050;

	private string _username = "6a8e4a92-585b-468c-b9b8-1d67271d3dad";
	private string _password = "pv5YKpyc3Mvw";
	private string _url = "https://stream.watsonplatform.net/speech-to-text/api";

	private string _workspaceId = null;

	private static string _authenticationToken = null;
	private bool _receivedAuthToken = false;
	private string _conversationVersionDate = "2017-05-26";

	private SpeechToText m_SpeechToText ;
	public bool IsWaitingArea;
		//public SpeechToTextWidget speechWidget;
		//public MicrophoneWidget micWidget;
	// Use this for initialization
	void Start () 
	{
        WebGLSpeechRecognition.Instance.SpeechRecognizedSuccessEvent += ParseSpeechResponse; 

        Credentials credentials = new Credentials(_username, _password,_url);

		m_SpeechToText = new SpeechToText(credentials);
        print(" ***** VoiceHandler - Sending Credentials");
		LogSystem.InstallDefaultReactors();
		///Runnable.Run(Example()); // Disable Watson for now
//
//
//		webSocket = new WebSocket (new Uri ("wss://stream.watsonplatform.net/speech-to-text/api/v1/recognize?watson-token="+_authenticationToken));
//		webSocket.OnOpen += OnWebSocketOpen;
//		webSocket.OnError += OnError;
//		webSocket.OnMessage += OnMessageReceived;
//		webSocket.OnBinary += OnBinaryMessageReceived;
//		webSocket.Open ();

		//		new Uri("http://icruits-env.p2avtnssms.us-west-2.elasticbeanstalk.com")

		//_speechRecognition = SpeechRecognitionModule.Instance;
		//		_speechRecognition.SpeechRecognizedSuccessEvent += SpeechRecognizedSuccessEventHandler;
		//		_speechRecognition.SpeechRecognizedFailedEvent += SpeechRecognizedFailedEventHandler;
		//_speechRecognition.FinishRecordEvent += SaveAudioClipTobackend;

		//		_speechRecognitionState.color = Color.white;
		//		sManager = new SocketManager (new System.Uri ("http://35.164.70.229:8001/socket.io/"));
		//		mainSocket = sManager.GetSocket ();
		//		SetupServer ();
		//		_startRecordButton.onClick.AddListener(StartRecordButtonOnClickHandler);
		//		_stopRecordButton.onClick.AddListener(StopRecordButtonOnClickHandler);
		//startRecordSlider.OnBarFilled += OnStartRecordVRSlider;
		//stopRecordSlider.OnBarFilled += OnStopRecordVRSlider;
        //		iScript.onSecuirtyClipRecorded += OnSecurityClipCompleted;
        //		keyString = "change";
        //		CreateSessionInBackend ();
        if (vModelScript != null)
        {
            //var mats = vModelScript.renderer.materials;
            ////mats[0] = AppController.instance.GetMaterial(AppController.instance.selectActors[0]);
            vModelScript.renderer.material = AppController.instance.GetMaterial(AppController.instance.selectActors[0]);
            vModelScript.renderer.material.mainTexture = AppController.instance.GetRenderTexture(0);
            vModelScript.GetComponentInChildren<VideoPlayer>().targetTexture = AppController.instance.GetRenderTexture(0);

            //vModelScript.renderer.material = ;
            vModelScript.ChangeToVideo (AppController.instance.selectActors[0]);
        }

        if (AppController.instance.selectActors.Length < 2)
        {
            if (v2ModelScript) v2ModelScript.gameObject.SetActive(false);
            v2ModelScript = null;
        }
        if (v2ModelScript != null)
        {
            v2ModelScript.renderer.material = AppController.instance.GetMaterial(AppController.instance.selectActors[1]);
            v2ModelScript.renderer.material.mainTexture = AppController.instance.GetRenderTexture(1);
            v2ModelScript.GetComponentInChildren<VideoPlayer>().targetTexture = AppController.instance.GetRenderTexture(1);

            v2ModelScript.ChangeToVideo(AppController.instance.selectActors[1]);
        }

        if (AppController.instance.selectActors.Length < 3)
        {
            if (v3ModelScript) v3ModelScript.gameObject.SetActive(false);
            v3ModelScript = null;
        }
        if (v3ModelScript != null)
        {
            v3ModelScript.renderer.material = AppController.instance.GetMaterial(AppController.instance.selectActors[2]);
            v3ModelScript.renderer.material.mainTexture = AppController.instance.GetRenderTexture(2);
            v3ModelScript.GetComponentInChildren<VideoPlayer>().targetTexture = AppController.instance.GetRenderTexture(2);
            v3ModelScript.ChangeToVideo(AppController.instance.selectActors[2]);
        }
        //		SendMicRecordStream ();
        //CheckMicrophones ();

		//		OpenSSE ();
		//		sse.On ("change_video", OnChangeVideo);
		//		_startRuntimeDetection.onClick.AddListener(StartRuntimeDetectionButtonOnClickHandler);
		//		_stopRuntimeDetection.onClick.AddListener(StopRuntimeDetectionButtonOnClickHandler);
		//		_isRuntimeDetectionToggle.onValueChanged.AddListener(IsRuntimeDetectionOnValueChangedHandler);
		//		_speechRecognition.StartRuntimeRecord();
		//		m_AudioClip = Microphone.Start (_microphoneDevice, true, 1, 16000);
		//		m_AudioClip = sampleClip;
		//		m_SpeechToText.Recognize(m_AudioClip, HandleOnRecognize);
		//		m_SpeechToText.Recognize (m_AudioClip, HandleOnRecognize);
		_speechRecognitionResult.text = "Before Start Invoke";





		if (!IsWaitingArea) 
		{
			isSecurityCleared = true;
			print ("in meeting room");
		} 
		else if (!uiHandler.scenarioScreen.activeSelf)
		{
			print ("in waiting room");
			isSecurityCleared = false;
			Invoke ("WatsonSocketListener", 1.0f);
		}
	}

    

    private IEnumerator Example()
	{
        print(".. Get Token");
		//  Get token
		if (!IBM.Watson.DeveloperCloud.Utilities.Utility.GetToken(OnGetToken, _url, _username, _password))
			Log.Debug("ExampleGetToken.GetToken()", "Failed to get token.");

		while (!_receivedAuthToken)
			yield return null;
	}

	private void OnGetToken(AuthenticationToken authenticationToken, string customData)
	{
		_authenticationToken = authenticationToken.Token;
		print ("string auth" + _authenticationToken);
		webSocket = new WebSocket (new Uri ("wss://stream.watsonplatform.net/speech-to-text/api/v1/recognize?watson-token="+authenticationToken.Token));
		webSocket.OnOpen += OnWebSocketOpen;
		webSocket.OnError += OnError;
		webSocket.OnMessage += OnMessageReceived;
		webSocket.OnBinary += OnBinaryMessageReceived;
        //webSocket.StartPingThread = true;
        //webSocket.PingFrequency = 200;
		webSocket.Open ();
		_receivedAuthToken = true;
	}

	private IEnumerator GetTokenTimeRemaining(float time)
	{
		yield return new WaitForSeconds(time);
		//		Log.Debug("ExampleGetToken.GetTokenTimeRemaining()", "created: {0} | time to expiration: {1} minutes | token: {2}", _authenticationToken.Created, _authenticationToken.TimeUntilExpiration, _authenticationToken.Token);
	}

	private void OnFail(RESTConnector.Error error, Dictionary<string, object> customData)
	{
		Log.Error("ExampleGetToken.OnFail()", "Error received: {0}", error.ToString());
	}

	void Update () {
		sessionDebugText.text = testText;
		//		CheckRecieve();
		if (iScript.isInterationCompleted) 
		{
			print ("1");
            if (SceneSetupScript.taskLevel == 1)
            {
                StartCoroutine(GetFileNameFromConvApi("Hello Dana"));
                AppController.instance.DownloadTicketImage();
            }
            else if (SceneSetupScript.taskLevel == 4)
                StartCoroutine(GetFileNameFromConvApi("Hello how are you doing"));
            else Invoke("WatsonSocketListener", 0.5f);
			iScript.isInterationCompleted = false;

		}

		if (vModelScript && vModelScript.isVideoEnded)
		{
			if (isInitialVideo) {
				isInitialVideo = false;
			}
			else {
				if (isContinousVideo || isContinousVideo2) 
				{
                    string clip;
                    if (isContinousVideo)
                    {
					    isContinousVideo = false;
                        clip = continousVideoClip;
                    }
                    else
                    {
                        isContinousVideo2 = false;
                        Debug.Log("Clip 3: " + continousVideoClip2);
                        clip = continousVideoClip2;
                    }
                    int no = int.Parse(clip.Substring(clip.Length - 1));
                    clip = clip.Substring(0, clip.Length - 2);

                    var model = vModelScript;
                    if (no == 2 && v2ModelScript != null)
                        model = v2ModelScript;
                    else if (no == 3 && v3ModelScript != null)
                        model = v3ModelScript;
                    model.ChangeToVideoCode(clip);
				}
                else if (isEndClip)
                {
                    //play end interaction
                    iScript.EndUserInteraction(true);
                }
                else if (isPauseClip)
                {
                    //pause interaction
                    iScript.PauseUserInteraction();
                }
                else 
				{
					_speechRecognitionResult.text = "In videomodel ended code update";
					inRoomTestData.text = "In videomodel ended code update";
					Invoke ("WatsonSocketListener", 0.1f);
					print ("2");
				}
			}
			vModelScript.isVideoEnded = false;
		}
		if(v2ModelScript != null)
		if (v2ModelScript.isVideoEnded) {
			if (isInitialVideo2) 
			{
				isInitialVideo2 = false;
			} 
			else 
			{
                    if (isContinousVideo || isContinousVideo2)
                    {
                        string clip;
                        if (isContinousVideo)
                        {
                            isContinousVideo = false;
                            clip = continousVideoClip;
                        }
                        else
                        {
                            isContinousVideo2 = false;
                            Debug.Log("Clip 3: " + continousVideoClip);
                            clip = continousVideoClip2;
                        }
                        int no = int.Parse(clip.Substring(clip.Length - 1));
                        clip = clip.Substring(0, clip.Length - 2);

                        var model = vModelScript;
                        if (no == 2 && v2ModelScript != null)
                            model = v2ModelScript;
                        else if (no == 3 && v3ModelScript != null)
                            model = v3ModelScript;
                        model.ChangeToVideoCode(clip);
                    }
                    else if (isEndClip)
                    {
                        //play end interaction
                        iScript.EndUserInteraction(true);
                    }
                    else if (isPauseClip)
                    {
                        //pause interaction
                        iScript.PauseUserInteraction();
                    }
                    else {
					_speechRecognitionResult.text = "In videomodel ended code update";
					inRoomTestData.text = "In videomodel ended code update";
					Invoke ("WatsonSocketListener", 0.1f);
					print ("3");
				}
			}
			v2ModelScript.isVideoEnded = false;
		}

        if (v3ModelScript != null)
            if (v3ModelScript.isVideoEnded)
            {               
                //else
                {
                    if (isContinousVideo || isContinousVideo2)
                    {
                        string clip;
                        if (isContinousVideo)
                        {
                            isContinousVideo = false;
                            clip = continousVideoClip;
                        }
                        else
                        {
                            isContinousVideo2 = false;
                            Debug.Log("Clip 3: " + continousVideoClip);
                            clip = continousVideoClip2;
                        }
                        int no = int.Parse(clip.Substring(clip.Length - 1));
                        clip = clip.Substring(0, clip.Length - 2);

                        var model = vModelScript;
                        if (no == 2 && v2ModelScript != null)
                            model = v2ModelScript;
                        else if (no == 3 && v3ModelScript != null)
                            model = v3ModelScript;
                        model.ChangeToVideoCode(clip);
                    }
                    else if (isEndClip)
                    {
                        //play end interaction
                        iScript.EndUserInteraction(true);
                    }
                    else if (isPauseClip)
                    {
                        //pause interaction
                        iScript.PauseUserInteraction();
                    }
                    else
                    {
                        _speechRecognitionResult.text = "In videomodel ended code update";
                        inRoomTestData.text = "In videomodel ended code update";
                        Invoke("WatsonSocketListener", 0.1f);
                        print("4");
                    }
                }
                v3ModelScript.isVideoEnded = false;
            }
    }

	//	void HandleOnSecRecognize (SpeechRecognitionEvent result)
	//	{
	////		_speechRecognitionResult.text = "Handle on security recognize is called";
	////		iScript.ShowDemo ();
	//		if (result != null && result.results.Length > 0)
	//		{
	//			foreach( var res in result.results )
	//			{
	//				foreach( var alt in res.alternatives )
	//				{
	////					iScript.ShowDemo ();
	//					string text = alt.transcript;
	//					_speechRecognitionResult.text = string.Format ("{0} ({1}, {2:0.00})\n", text, res.final ? "Final" : "Interim", alt.confidence);
	//					Debug.Log(string.Format( "{0} ({1}, {2:0.00})\n", text, res.final ? "Final" : "Interim", alt.confidence));
	////					ValidateSecCode (text);
	//				}
	//			}
	//		}
	//	}

	void HandleOnStreamRecognize (SpeechRecognitionEvent result) { // Not Used ? !!
		_speechRecognitionResult.text = "Handle on Stream recognize is called";
		if (result != null && result.results.Length > 0)
		{
			foreach( var res in result.results )
			{
				foreach( var alt in res.alternatives )
				{
					string text = alt.transcript;
					_speechRecognitionResult.text = string.Format ("{0} ({1}, {2:0.00})\n", text, res.final ? "Final" : "Interim", alt.confidence);
					Debug.Log(string.Format( "{0} ({1}, {2:0.00})\n", text, res.final ? "Final" : "Interim", alt.confidence));
					resulttextForConvApi = text;
					if (isSecurityCleared) 
					{
						inRoomTestData.text = string.Format ("{0} ({1}, {2:0.00})\n", text, res.final ? "Final" : "Interim", alt.confidence);
						print ("get file name from conv api");
						StartCoroutine (GetFileNameFromConvApi (text));
					} else 
					{
						uiHandler.StartSecLoading ();
						StartCoroutine (GetSecurityAccess (text));
					}
				}
			}
		}
	}

	bool IsCharacter2SpeechList(string text)
	{
		string clipname = "IcruitsSales_cut_";

		if (text.Contains("John") || text.Contains("Joan") || text.Contains("Jaon") )
		{
			v2ModelScript.ChangeToVideoCode (clipname + "001");
			return true;
		} 
		else if (text.Contains("how are you John")|| text.Contains("how are you Joan") || text.Contains("how are you Jaon")) 
		{
			v2ModelScript.ChangeToVideoCode (clipname + "002");
			return true;
		}
		return false;
	}

	public IEnumerator GetFileNameFromConvApi (string text) 
	{
		print("--> "+text+"*");
		
		//if(!IsCharacter2SpeechList(text))
		//{
		WWWForm form = new WWWForm();
		form.AddField ("appl_request", text);
        form.AddField("user", AppController.instance.authInfo.username);
        form.AddField("context", AppController.instance.conversationState);
        form.AddField("scene", AppController.instance.conversationScene);
        form.AddField("wait_message", AppController.instance.conversationWaitMsg);
        form.AddField("wait_timer", AppController.instance.conversationWaitTimer);
        //		form.headers.Add ("Content-Type", "application/json");
        //		Dictionary<string,string> headers = new Dictionary<string, string>();
        //		headers.Add("Content-Type", "application/json");
        //		string ourPostData = string.Format ("{\"appl_request\":\"{0}\"", text);
        //		byte[] pData = System.Text.Encoding.ASCII.GetBytes(ourPostData.ToCharArray());
        //		UnityWebRequest www = UnityWebRequest.Post("https://icruitsttest.mybluemix.net/testChat", pData, headers);
        string adr = AppController.instance.authInfo.environment[1].scenarios[AppController.instance.CurrentLevel].url; //"https://icruitsdemo.mybluemix.net/icruitsdemo2";
        if (SceneSetupScript.taskLevel==4) adr = AppController.instance.authInfo.environment[0].scenarios[AppController.instance.CurrentLevel].url;//"https://icruitsdemo.mybluemix.net/icruitsdemo"; // WareHouse Conv

            UnityWebRequest www = UnityWebRequest.Post(adr, form);

		yield return www.SendWebRequest();
		if(www.isNetworkError || www.isHttpError) 
		{
			Debug.Log ("conv api error");
			Debug.Log(www.error);
			_speechRecognitionResult.text = www.error;
			inRoomTestData.text = www.error;
		}
		//	else if(IsWaitingArea && (text.Contains ("Yes") || text.Contains ("yes")))
		//{
		//		print ("show instruction");
		//		uiHandler.DisplayInstructionUI ();	
		//		uiHandler.VideoScreen.SetActive (false);
		//}
		else 
		{
            string respResult = www.downloadHandler.text;

			print(respResult);
            var jsn = JSON.Parse(respResult);
            Debug.Log("*** "+jsn["outputs"].Value);
            AppController.instance.conversationState = jsn["context"].ToString();

            VideoModelScript model;
				
			string [] respArray = jsn["outputs"].Value.Split ('=');
								
			if (respArray.Length > 1) {
                AppController.instance.videoText = respArray[0].ToLower();
                //Debug.Log ("Post conversation api complete! " + respArray [1]);
                //			respArray [1] = "XXXAAAXX2000V01";
                _speechRecognitionResult.text = respArray [1];
				inRoomTestData.text = respArray [1];
                if (respArray[1].Contains("IdleClip")) // Init actors
                {
                    AppController.instance.SetActors(respArray[1]);
                }
				//else if (respArray [1].Contains ("XXXAAA"))
    //            {
    //                        isEndClip = true;
    //                string[] clipNames = respArray[1].Split(';');
    //                int no = int.Parse( clipNames[0].Substring(clipNames[0].Length - 1));
    //                clipNames[0]=clipNames[0].Substring(0,clipNames[0].Length - 2);
                        
    //                model = vModelScript;
    //                if (no == 2 && v2ModelScript != null)
    //                    model = v2ModelScript;
    //                else if (no == 3 && v3ModelScript != null)
    //                     model = v3ModelScript;
    //                model.ChangeToVideoCode(clipNames[0]);
    //            }
                if (respArray [1].Contains (";") /*&& !respArray[1].Contains(";XXXAAA")*/ && !respArray[1].Contains("IdleClip")) {

                    if (respArray[1].Contains("XXXAAA")) isEndClip = true;
                    if (respArray[1].Contains("TIMEOUTXXX")) isPauseClip = true;

                    string[] clipNames = respArray [1].Split (';');

                    if (clipNames.Length>1 && clipNames[1].Length>0 && !clipNames[1].Contains("XXXAAA"))
                    {
					    isContinousVideo = true;
						continousVideoClip = clipNames [1];

                    }
                    if (clipNames.Length > 2 && clipNames[2].Length > 0 && !clipNames[2].Contains("XXXAAA"))
                    {
                        isContinousVideo2 = true;
                        continousVideoClip2 = clipNames[2];
                    }

                    int no = int.Parse(clipNames[0].Substring(clipNames[0].Length - 1));
                    clipNames[0] = clipNames[0].Substring(0, clipNames[0].Length - 2);
                    Debug.Log("player: " +no);
                    model = vModelScript;
                    if (no == 2 && v2ModelScript != null)
                        model = v2ModelScript;
                    else if (no == 3 && v3ModelScript != null)
                        model = v3ModelScript;
                    model.ChangeToVideoCode (clipNames [0]);
				} else if (!respArray[1].Contains("XXXAAA") && !respArray[1].Contains("IdleClip"))
                {
                    int no = int.Parse(respArray[1].Substring(respArray[1].Length - 1));
                    string clip = respArray[1].Substring(0, respArray[1].Length - 2);

                    model = vModelScript;
                    if (no == 2 && v2ModelScript != null)
                        model = v2ModelScript;
                    else if (no == 3 && v3ModelScript != null)
                        model = v3ModelScript;

                    model.ChangeToVideoCode(clip.TrimStart(' '));
				}
				InactivateSpeechListener ();
			}
//			} else 
//			{
//				print ("********* -->  no reply ");
//				int num = UnityEngine.Random.Range (10, 099);
//				string clipname = "IcruitsSales_cut_0" + num;
//				print ("playing "+clipname);
//				v2ModelScript.ChangeToVideoCode (clipname);
//			}
			//			StopRecording ();
			//			CancelInvoke ("RecordAndSendChunks");
			//			Microphone.End(_microphoneDevice);
			}

            //SpeechRecognitionModule.Instance.StartRuntimeRecord();

        //}
    }

	IEnumerator GetSecurityAccess (string text) {
		print (" >>>>>>> "+text);
		//const string passcode = "Hi";
		_speechRecognitionResult.text = "In Security post";
		WWWForm form = new WWWForm();
		form.AddField ("appl_request", text.ToLower().TrimEnd(new char[] {' '}));
		UnityWebRequest www = UnityWebRequest.Post("https://icruitsdemo.mybluemix.net/icruits/auth1", form);
		yield return www.Send();
		if (www.isNetworkError) {
			Debug.Log ("conv api error");
			Debug.Log (www.error);
			_speechRecognitionResult.text = www.error;
            WebGLSpeechRecognition.Instance.StartRecord();
            //SpeechRecognitionModule.Instance.StartRuntimeRecord();

            uiHandler.StopSecLoading ();
		}
        else
        {
            Debug.Log("Auth Resp: " + www.downloadHandler.text);
            if (www.downloadHandler.text.Contains("null"))
            {
                uiHandler.StopSecLoading(true);
                WebGLSpeechRecognition.Instance.StartRecord();
                //SpeechRecognitionModule.Instance.StartRuntimeRecord();

                yield break;
            }

            AppController.instance.authInfo = AuthInfo.CreateFromJSON(www.downloadHandler.text);
            AppController.instance.canvas.SetActive(AppController.instance.authInfo.debug);
            AppController.instance.speechDelay = AppController.instance.authInfo.speechpause / 800f;
            int no = AppController.instance.authInfo.jobscenario_ID;
            AppController.instance.DownloadTicketImage(true);
            //Int32.TryParse(www.downloadHandler.text, out no);

            if (no>=0)
            {
                print("came here for passing the code");

                AppController.instance.CurrentLevel = no;
                AppController.instance.savedPass = text.ToLower().TrimEnd(new []{' '});

                //AppController.instance.conversationState = AppController.instance.authInfo.environment[0].scenarios[no].conversation_state;
                AppController.instance.conversationScene = AppController.instance.authInfo.environment[0].scenarios[no].scene;
                AppController.instance.conversationWaitMsg = AppController.instance.authInfo.environment[0].scenarios[no].wait_message;
                AppController.instance.conversationWaitTimer = AppController.instance.authInfo.environment[0].scenarios[no].wait_timer;

                uiHandler.SetupEnvironmentUI(no);
			    uiHandler.StopSecLoading ();
			    uiHandler.DisplayUnlock ();
			    yield return new WaitForSeconds (0.5f);
			    string respResult = www.downloadHandler.text;
			    _speechRecognitionResult.text = respResult;
			    iScript.ShowDemo ();
			    isSecurityCleared = true;
			    InactivateSpeechListener ();
            }
            else
            {
                print("password wrong");
                uiHandler.StopSecLoading(true);
                WebGLSpeechRecognition.Instance.StartRecord();
                //SpeechRecognitionModule.Instance.StartRuntimeRecord();
            }
        }

    }

    private void watsonSpeechError ( string error) {
		_speechRecognitionResult.text = error;
		//		iScript.ShowDemo ();
	}

	public void InactivateSpeechListener () {
        WebGLSpeechRecognition.Instance.StopRecord();
        //SpeechRecognitionModule.Instance.StopRuntimeRecord();
		//speechWidget.Active = false;
		//micWidget.DeactivateMicrophone ();
	}

	public void WatsonSocketListener () {
		print ("-->watson socket listener");
        //StartCoroutine( GetSecurityAccess("national gaurd"));
        //		AudioClip audC = sampleClip;
        //		byte[] buffer;
        //		PCMWrapper.AudioClipToPCMBytesArray(audC, out buffer);
        //		webSocket.Send (buffer);
        //		Debug.Log ("sent buffer to web socket");
        //		_speechRecognitionResult.text = "In WatsonSocketListener taskLevel :: " + iScript.taskLevel.ToString ();
        //		inRoomTestData.text = "In WatsonSocketListener";
        //		m_AudioClip = Microphone.Start (_microphoneDevice, true, 1, 44100);
        //		if (!isListen) {
        //			isListen = m_SpeechToText.StartListening (HandleOnStreamRecognize);
        //			m_SpeechToText.OnError += watsonSpeechError;
        //		}
        //		uiHandler.StartSecLoading ();
        //		uiHandler.DisplayUnlock ();
        //		m_SpeechToText.Recognize (audC, HandleOnSecRecognize);
        //		InvokeRepeating ("SendClipData", 1, 1.0f);
        //		isRecord = false;
        //		InvokeRepeating ("RecordAndSendChunks", 0.3f, 4.0f);
        //speechWidget.Active = true;
        //if (micWidget.Disable) {
        //micWidget.ActivateMicrophone ();
        //}
        //		Active = true;
        //		StartRecording ();
        WebGLSpeechRecognition.Instance.StartRecord();
        //SpeechRecognitionModule.Instance.StartRuntimeRecord();
	}

    private void ParseSpeechResponse(string resp)  //Google Speech Handling
    {
        if (resp != "")
        {
            OnSpeechText(resp);
        }
        else
            WebGLSpeechRecognition.Instance.StartRecord();
            //SpeechRecognitionModule.Instance.StartRuntimeRecord();
    }

    public void OnSpeechText (string text) //Sends Speech Texts to bluemix
	{
        print("Got Speech: " + text);
		resulttextForConvApi = text;
		if (isSecurityCleared) 
		{
			inRoomTestData.text = text;
			StartCoroutine (GetFileNameFromConvApi (text));
		} else 
		{
			uiHandler.StartSecLoading ();
			StartCoroutine (GetSecurityAccess (text));
		}
	}

	public bool Active
	{
		get { return m_SpeechToText.IsListening; }
		set
		{
			if (value && !m_SpeechToText.IsListening)
			{
				m_SpeechToText.DetectSilence = true;
				m_SpeechToText.EnableWordConfidence = false;
				m_SpeechToText.EnableTimestamps = false;
				m_SpeechToText.SilenceThreshold = 0.03f;
				m_SpeechToText.MaxAlternatives = 1;
				//				m_SpeechToText.EnableContinousRecognition = true;
				m_SpeechToText.EnableInterimResults = true;
				m_SpeechToText.OnError = OnError;
				//				m_SpeechToText.StartListening(HandleOnStreamRecognize);
			}
			else if (!value && m_SpeechToText.IsListening)
			{
				//				m_SpeechToText.StopListening();
			}
		}
	}

	//private void StartRecording()
	//{
	//	if (m_RecordingRoutine == 0)
	//	{
	//		UnityObjectUtil.StartDestroyQueue();
	//		m_RecordingRoutine = Runnable.Run(RecordingHandler());
	//	}
	//}

	//private void StopRecording()
	//{
	//	if (m_RecordingRoutine != 0)
	//	{
	//		Microphone.End(m_MicrophoneID);
	//		Runnable.Stop(m_RecordingRoutine);
	//		m_RecordingRoutine = 0;
	//	}
	//}

	private void OnError(string error)
	{
		Active = false;

		Log.Debug("ExampleStreaming", "Error! {0}", error);
	}

	//	private void OnSecurityClipCompleted () {
	////		iScript.ShowDemo ();
	//		AudioClip secClip = iScript.securityClip;
	//		m_SpeechToText.Recognize (secClip, HandleOnSecRecognize);
	//	}
	//
	//	private void SendClipData () {
	//		if (isListen) {
	//			m_AudioData = new AudioData (m_AudioClip, 0.3f);
	//			m_SpeechToText.OnListen (m_AudioData);
	//		} 
	//	}

	//private IEnumerator RecordingHandler()
	//{
	//	Log.Debug("ExampleStreaming", "devices: {0}", Microphone.devices);
	//	m_Recording = Microphone.Start(m_MicrophoneID, true, m_RecordingBufferSize, m_RecordingHZ);
	//	yield return null;      // let m_RecordingRoutine get set..

	//	if (m_Recording == null)
	//	{
	//		StopRecording();
	//		yield break;
	//	}

	//	bool bFirstBlock = true;
	//	int midPoint = m_Recording.samples / 2;
	//	float[] samples = null;

	//	while (m_RecordingRoutine != 0 && m_Recording != null)
	//	{
	//		int writePos = Microphone.GetPosition(m_MicrophoneID);
	//		if (writePos > m_Recording.samples || !Microphone.IsRecording(m_MicrophoneID))
	//		{
	//			Log.Error("MicrophoneWidget", "Microphone disconnected.");

	//			StopRecording();
	//			yield break;
	//		}

	//		if ((bFirstBlock && writePos >= midPoint)
	//			|| (!bFirstBlock && writePos < midPoint))
	//		{
	//			// front block is recorded, make a RecordClip and pass it onto our callback.
	//			samples = new float[midPoint];
	//			m_Recording.GetData(samples, bFirstBlock ? 0 : midPoint);

	//			AudioData record = new AudioData();
	//			record.MaxLevel = Mathf.Max(samples);
	//			record.Clip = AudioClip.Create("Recording", midPoint, m_Recording.channels, m_RecordingHZ, false);
	//			record.Clip.SetData(samples, 0);
	//			byte[] ba;
	//			PCMWrapper.FloatSamplesToPCMBytesArray (samples, m_Recording, out ba);
	//			webSocket.Send (ba);
	//			//				m_SpeechToText.OnListen(record);

	//			bFirstBlock = !bFirstBlock;
	//		}
	//		else
	//		{
	//			// calculate the number of samples remaining until we ready for a block of audio, 
	//			// and wait that amount of time it will take to record.
	//			int remaining = bFirstBlock ? (midPoint - writePos) : (m_Recording.samples - writePos);
	//			float timeRemaining = (float)remaining / (float)m_RecordingHZ;

	//			yield return new WaitForSeconds(timeRemaining);
	//		}

	//	}

	//	yield break;
	//}

	private void OnDisable()
	{
		//		sse.Close ();
		//		CloseSSE();
		//		iScript.onSecuirtyClipRecorded -= OnSecurityClipCompleted;
		//		_speechRecognition.SpeechRecognizedSuccessEvent -= SpeechRecognizedSuccessEventHandler;
		//		_speechRecognition.SpeechRecognizedFailedEvent -= SpeechRecognizedFailedEventHandler;

		//startRecordSlider.OnBarFilled -= OnStartRecordVRSlider;
		//stopRecordSlider.OnBarFilled -= OnStopRecordVRSlider;
        WebGLSpeechRecognition.Instance.SpeechRecognizedSuccessEvent -= ParseSpeechResponse;

        //		_speechRecognition.StopRuntimeRecord();
    }

    /// <summary>
    /// Old Code currently not in use
    /// </summary>
    /// <param name="webSocket">Web socket.</param>

    private void OnWebSocketOpen(WebSocket webSocket)
	{
		testText = "WebSocket Open!";
		//Debug.Log("WebSocket Open!");
		//		StartCoroutine (GetFileNameFromConvApi ("hi there"));
		//		TestAudioSocket ();
		//		SendMicRecordStream ();
	}

	private void OnMessageReceived(WebSocket webSocket, string message)
	{
		print ("speech **"+message+"**");
		testText = message;
		if (_speechRecognitionResult) _speechRecognitionResult.text = message;
		Debug.Log("Text Message received from server: " + message);
		resulttextForConvApi = message;
        if (message.Contains("error") && message.Contains("Session timed out")) return;
		if (isSecurityCleared) {
			if (inRoomTestData) inRoomTestData.text = message;
			StartCoroutine (GetFileNameFromConvApi (message));
		} else {
			uiHandler.StartSecLoading ();
			StartCoroutine (GetSecurityAccess (message));
		}
	}

	private void OnBinaryMessageReceived(WebSocket webSocket, byte[] message)
	{
		Debug.Log("Binary Message received from server. Length: " + message.Length);
	}

	private void OnError(WebSocket ws, Exception ex)
	{
		string errorMsg = string .Empty;
#if !UNITY_WEBGL
        if (ws.InternalRequest.Response != null)
			errorMsg = string.Format("Status Code from Server: {0} and Message: {1}",
				ws.InternalRequest.Response.StatusCode,
				ws.InternalRequest.Response.Message);
#endif
        Debug.Log("An error occured: " + (ex != null ? ex.Message : "Unknown: " + errorMsg));
        //if (ex != null && ex.Message.Contains("TCP Stream closed"))
        //    Runnable.Run(Example());
        testText = errorMsg;
	}

	void OpenSSE (){
		sse = new EventSource (new System.Uri ("http://icruits-env-1.us-west-2.elasticbeanstalk.com//change_video"));
		sse.Open ();
	}

	void CloseSSE() {
		sse.OnOpen -= OnEventSourceOpened;
		sse.OnMessage -= OnEventSourceMessage;
		sse.OnError -= OnEventSourceError;
		sse.OnClosed -= OnEventSourceClosed;
		sse.Close ();
	}
	void OnEventSourceOpened(EventSource source)
	{

		Debug.Log("EventSource Opened!");
	}

	void OnEventSourceMessage(EventSource source, Message msg)
	{
		//		messageEvent.text = "Message: " + msg.Data;
		vModelScript.ChangeToVideo (msg.Data);
		Debug.Log("Message: " + msg.Data);
		//		CloseSSE ();
		OpenSSE ();
	}

	void OnEventSourceError(EventSource source, string error)
	{
		Debug.Log("Error: " + error);
	}

	void OnEventSourceClosed(EventSource source)
	{
		Debug.Log("EventSource Closed!");
	}

	void CreateSessionInBackend () {
		StartCoroutine (CreateSession ());
		InvokeRepeating ("CurrentClip", 1.0f, 1.0f);
	}

	void ChangeVideo (string name) {
		//		vModelScript.ChangeToVideo (name);
	}

	IEnumerator CreateSession () {
		WWWForm form = new WWWForm();
		form.AddField ("job_profile", jobProfile);
		UnityWebRequest www = UnityWebRequest.Post("http://icruits-env-1.us-west-2.elasticbeanstalk.com//sessions", form);
		yield return www.Send();
		if(www.isNetworkError) {
			Debug.Log(www.error);
			//			session.text = www.error;
		}
		else {
			currentSessionId = www.downloadHandler.text;
			//			session.text = currentSessionId;
			Debug.Log("Form upload complete!");
		}
	}

	void CurrentClip () {
		StartCoroutine (GetCurrentClip ());
	}

	IEnumerator GetCurrentClip () {
		UnityWebRequest www = UnityWebRequest.Get ("http://icruits-env-1.us-west-2.elasticbeanstalk.com//current_clip?id=" + currentSessionId);
		yield return www.Send();
		if (www.isNetworkError) {
			//			Debug.Log (www.error);
		} else {
			string temp = www.downloadHandler.text;
			if (!temp.Equals ("")) {
				if (temp.Equals ("end_session")) {
					// Trigger session end
					if (!isSessionEnded) {
						//						iScript.EndUserInteraction ();
						isSessionEnded = true;
					}
				} else if (!currentClip.Equals(temp)) {
					currentClip = temp;

					ChangeVideo (currentClip);
				} else {

				}
			}
			Debug.Log (temp);
		}
	}

	//private void StartRuntimeDetectionButtonOnClickHandler()
	//{
	//	//		ApplySpeechContextPhrases();

	//	_startRuntimeDetection.interactable = false;
	//	_stopRuntimeDetection.interactable = true;
	//	_speechRecognitionState.color = Color.green;
	//	//		_speechRecognitionResult.text = "";
	//	_speechRecognition.StartRuntimeRecord();
	//}

	//private void StopRuntimeDetectionButtonOnClickHandler()
	//{
	//	_stopRuntimeDetection.interactable = false;
	//	_startRuntimeDetection.interactable = true;
	//	_speechRecognitionState.color = Color.green;
	//	_speechRecognition.StopRuntimeRecord();
	//	//		_speechRecognitionResult.text = "";
	//}

	//private void StartRecordButtonOnClickHandler()
	//{
	//	_startRecordButton.interactable = false;
	//	_stopRecordButton.interactable = true;
	//	_speechRecognitionState.color = Color.red;
	//	//		_speechRecognitionResult.text = "";
	//	_speechRecognition.StartRecord();
	//}

	//private void OnStartRecordVRSlider () {
	//	_speechRecognitionState.color = Color.red;
	//	//		_speechRecognitionResult.text = "Started Recording";
	//	//		_speechRecognition.StartRuntimeRecord();
	//}

	//private void OnStopRecordVRSlider () {
	//	_speechRecognitionState.color = Color.yellow;
	//	//		_speechRecognitionResult.text = "";
	//	//		_speechRecognition.StopRuntimeRecord();
	//}

	//private void StopRecordButtonOnClickHandler()
	//{
	//	//		ApplySpeechContextPhrases();

	//	_stopRecordButton.interactable = false;
	//	_speechRecognitionState.color = Color.yellow;
	//	_speechRecognition.StopRecord();

	//}

	private void SendAudioClipToBackend() {
		StartCoroutine (UploadAudioClip ());
	}

	IEnumerator UploadAudioClip() {
		//		byte[] bytes = Encoding.UTF8.GetBytes (_speechRecognition.GetRecordedAudio ());
		AudioClip audC = _speechRecognition.GetRecordedAudio ();
		//		AudioClip audC = sampleClip;
		//		float [] temp = new float[audC.samples * audC.channels];
		//		audC.GetData (temp, 0);
		//		byte[] bytes = new byte[temp.Length * 4];
		//		System.Buffer.BlockCopy(temp, 0, bytes, 0, bytes.Length);
		byte[] buffer;
		PCMWrapper.AudioClipToPCMBytesArray(audC, out buffer);
		//		System.BitConverter.GetBytes (temp)
		//		List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
		//		formData.Add( new MultipartFormDataSection("field1=" + _speechRecognitionResult.text) );
		//		formData.Add( new MultipartFormFileSection("myfiledata", "sampleClip") );
		WWWForm form = new WWWForm();
		form.AddField ("text", _speechRecognitionResult.text);
		form.AddField ("session_id", currentSessionId);
		form.AddBinaryData ("audio", buffer, "sample.wav"+currentSessionId, "audio/x-wav");


		//		UnityWebRequest www = UnityWebRequest.Get("http://icruits-env.p2avtnssms.us-west-2.elasticbeanstalk.com/user_unit_responses");
		UnityWebRequest www = UnityWebRequest.Post("http://icruits-env-1.us-west-2.elasticbeanstalk.com//user_unit_responses", form);
		yield return www.Send();

		if(www.isNetworkError) {
			Debug.Log(www.error);
		}
		else {
			if (_speechRecognitionResult.text.Contains (keyString)) {
				vModelScript.NextVideo ();
			}
			Debug.Log("Form upload complete!");
		}
	}

	private void ApplySpeechContextPhrases()
	{
		string[] phrases = _contextPhrases.text.Trim().Split(","[0]);

		if (phrases.Length > 0)
			_speechRecognition.SetSpeechContext(phrases);
	}

	//	// Update is called once per frame
	//	void Update () {
	////		_positionOfLastSample = Microphone.GetPosition(null);
	////		_micRecordClip.GetData(_checkingSamples, 0);
	//	}

	private void SaveAudioClipTobackend () {
		AudioClip audC = _speechRecognition.GetRecordedAudio ();
		byte[] buffer;
		PCMWrapper.AudioClipToPCMBytesArray(audC, out buffer);
	}

	//private void SpeechRecognizedFailedEventHandler(string obj)
	//{
	//	_speechRecognitionState.color = Color.green;
	//	//		_speechRecognitionResult.text = "Speech Recognition failed with error: " + obj;
	//	//		SendAudioClipToBackend();
	//	//		_startRecordButton.interactable = true;
	//	//		_stopRecordButton.interactable = false;
	//}

	//private void IsRuntimeDetectionOnValueChangedHandler(bool value)
	//{
	//	//StopRuntimeDetectionButtonOnClickHandler();

	//	(_speechRecognition as SpeechRecognitionModule).isRuntimeDetection = value;
	//}

	//private void SpeechRecognizedSuccessEventHandler(RecognitionResponse obj)
	//{
	//	//		_startRecordButton.interactable = true;

	//	_speechRecognitionState.color = Color.black;

	//	if (obj != null && obj.results.Length > 0)
	//	{
	//		//			_speechRecognitionResult.text = "Speech Recognition succeeded! Detected Most useful: " + obj.results[0].alternatives[0].transcript;

	//		string other = "\nDetected alternative: ";

	//		foreach (var result in obj.results)
	//		{
	//			foreach (var alternative in result.alternatives)
	//			{
	//				if (obj.results[0].alternatives[0] != alternative)
	//					other += alternative.transcript + ", ";
	//			}
	//		}

	//		//			_speechRecognitionResult.text += other;
	//	}
	//	else
	//	{
	//		//			_speechRecognitionResult.text = "Speech Recognition succeeded! Words are no detected.";

	//	}
	//	SendAudioClipToBackend();
	//}

	//private void SendMicRecordStream () {
	//	//		InvokeRepeating ("RecordAndSendChunks", 3.0f, 20f);
	//}
	////
	//private void RecordAndSendChunks () {
	//	if (!isRecord) {
	//		_micRecordClip = Microphone.Start (_microphoneDevice, true, 200, 16000);
	//		isRecord = true;
	//	} else {
	//		int pos = Microphone.GetPosition(_microphoneDevice);
	//		int diff = pos-lastSample;
	//		if (diff > 0) {
	//			float[] samples = new float[diff * _micRecordClip.channels];
	//			_micRecordClip.GetData (samples, lastSample);
	//			byte[] ba;
	//			PCMWrapper.FloatSamplesToPCMBytesArray (samples, _micRecordClip, out ba);
	//			//				byte[] ba = ToByteArray (samples);
	//			//				mySocket.BeginSend(ba, 0, ba.Length, SocketFlags.None, new AsyncCallback(WriterThread), mySocket);
	//			webSocket.Send (ba);
	//			Debug.Log(Microphone.GetPosition(null).ToString());
	//			Debug.Log(ba.Length.ToString());
	//			_speechRecognitionResult.text = "data sending length " + ba.Length.ToString () + " last sample " + lastSample.ToString ();
	//		}
	//		lastSample = pos;
	//	}
	//}

	//void CheckRecieve()
	//{
	//	string msg = string.Empty;
	//	lock(RecieveQueueLocker)
	//	{
	//		if (RecieveThreadFree && RecieveQueue.Count > 0)
	//		{
	//			msg = RecieveQueue.Dequeue();
	//		}
	//	}

	//	if (!string.IsNullOrEmpty(msg))
	//	{
	//		if (msg.Length >= 2)
	//		{
	//			Debug.Log ("Message: " + msg);

	//			//				_speechRecognitionResult.text = "Mesasge :" + msg;
	//			vModelScript.ChangeToVideoCode (msg);
	//			switch (msg.Substring(0,2))
	//			{
	//			case "G:": //Packet is for the GUI Manager
	//				//					clientgui.OnNetwork(msg.Substring(2));
	//				break;
	//			case "M:": //Packet is for the Map Manager
	//				break;
	//			default:
	//				break;
	//			}
	//		} else
	//		{
	//			Debug.Log("Invalid Message Length:"  + msg);
	//		}
	//	}
	//}

	//private void SetupServer () {
	//	int attempts = 0;

	//	try
	//	{
	//		attempts++;
	//		mySocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
	//		mySocket.Connect("35.164.70.229", 8001);
	//		Debug.Log ("Connected.");
	//		mySocket.BeginReceive(recievebuffer, 0, recievebuffer.Length, SocketFlags.None, new AsyncCallback(ReaderThread), mySocket);
	//		Debug.Log ("Reader Thread Started.");
	//		socketReady = true;
	//		//			return true;
	//	}
	//	catch (Exception e)
	//	{
	//		Console.WriteLine("Connection attempts: " + attempts.ToString());

	//	}
	//	//		return false;
	//}

	//private static void ReaderThread (IAsyncResult AR)
	//{
	//	try
	//	{
	//		//Socket socket = (Socket)AR.AsyncState;
	//		Socket socket = mySocket;
	//		int recieved = socket.EndReceive(AR);
	//		byte[] dataBuf = new byte[recieved];
	//		Array.Copy(recievebuffer, dataBuf, recieved);
	//		string text = Encoding.ASCII.GetString(dataBuf);
	//		lock(RecieveQueueLocker)
	//		{
	//			RecieveQueue.Enqueue(text);
	//		}
	//		try
	//		{
	//			mySocket.BeginReceive(recievebuffer, 0, recievebuffer.Length, SocketFlags.None, new AsyncCallback(ReaderThread), mySocket);
	//		}
	//		catch (Exception e)
	//		{
	//			Debug.Log("(Reader Thread: Start Next) Socket error: " + e);
	//			//clientgui.OnNetwork("ER:" + e);
	//		}
	//	}
	//	catch (Exception e)
	//	{
	//		Debug.Log("(Reader Thread: Begin) Socket error: " + e);
	//		//clientgui.OnNetwork("ER:" + e);

	//	}
	//}

	//private static void WriterThread (IAsyncResult AR)
	//{
	//	Socket socket = (Socket)AR.AsyncState;
	//	socket.EndSend(AR);
	//}



	void FixedUpdate () {
		//		if (!isRecord) {
		//			_micRecordClip = Microphone.Start (_microphoneDevice, true, 200, 16000);
		//			isRecord = true;
		//		} else {
		//			int pos = Microphone.GetPosition(_microphoneDevice);
		//			int diff = pos-lastSample;
		//			if (diff > 0) {
		//				float[] samples = new float[diff * _micRecordClip.channels];
		//				_micRecordClip.GetData (samples, lastSample);
		//				byte[] ba;
		//				PCMWrapper.FloatSamplesToPCMBytesArray (samples, _micRecordClip, out ba);
		////				byte[] ba = ToByteArray (samples);
		////				mySocket.BeginSend(ba, 0, ba.Length, SocketFlags.None, new AsyncCallback(WriterThread), mySocket);
		//				webSocket.Send (ba);
		////				Debug.Log(Microphone.GetPosition(null).ToString());
		////				Debug.Log(ba.Length.ToString());
		//				_speechRecognitionResult.text = "data sending length " + ba.Length.ToString () + " last sample " + lastSample.ToString ();
		//			}
		//			lastSample = pos;
		//		}
	}

	// Used to convert the audio clip float array to bytes
	public byte[] ToByteArray(float[] floatArray) {
		int len = floatArray.Length * 4;
		byte[] byteArray = new byte[len];
		int pos = 0;
		foreach (float f in floatArray) {
			byte[] data = System.BitConverter.GetBytes(f);
			System.Array.Copy(data, 0, byteArray, pos, 4);
			pos += 4;
		}
		return byteArray;
	}

	//private void CheckMicrophones()
	//{
	//	if (Microphone.devices.Length > 0) {
	//		_microphoneDevice = Microphone.devices [0];
	//	} else {
	//	}
	//}
}
