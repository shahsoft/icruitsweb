﻿using System.Collections;
using System.Collections.Generic;
using CrazyMinnow.SALSA;
using UnityEngine;

public class CustomShapeRandomEyes : MonoBehaviour
{
    [Header("Syncs RBlink with other Blink Shape L")]
    [SerializeField] private RandomEyes3D rEyes;

    private SkinnedMeshRenderer mesh;
    public string blendShape = "EyeBlink_R";

    private int shapeIndex;
    // Start is called before the first frame update
    void Start()
    {
       // rEyes = GetComponent<RandomEyes3D>();
        mesh = rEyes.skinnedMeshRenderer;
        shapeIndex = ShapeSearch(mesh, blendShape);
    }

    // Update is called once per frame
    void Update()
    {
        var blinkWeight = rEyes.lookAmount.blink;

        // Apply blink action on eye lids
        if (mesh)
        {
            if (shapeIndex != -1) mesh.SetBlendShapeWeight(shapeIndex, (blinkWeight * 90) / 100);
        }
    }
    
    public int ShapeSearch(SkinnedMeshRenderer skndMshRndr, string endsWith)
    {
        int index = -1;
        if (skndMshRndr)
        {
            for (int i=0; i<skndMshRndr.sharedMesh.blendShapeCount; i++)
            {
                if (skndMshRndr.sharedMesh.GetBlendShapeName(i).EndsWith(endsWith))
                {
                    index = i;
                    break;
                }
            }
        }
        return index;
    }
}
