﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;
using UnityEngine.UI;

public class EnvironmentController : MonoBehaviour {

	public VRInteractiveItem[] environmentButtons;
	public GameObject videoScreen;
    public UIHandler uiHandler;
	private bool m_BarFilled;                                           // Whether the bar is currently filled.
	private bool m_GazeOver;                                            // Whether the user is currently looking at the bar.
	private float m_Timer;                                              // Used to determine how much of the bar should be filled.
	private Coroutine m_FillBarRoutine;  
	private int m_curLvl = 1;

	/// <summary>
	/// Register all the event here.
	/// </summary>
	void OnEnable () 
	{
        // foreach(VRInteractiveItem btn in environmentButtons)
        // {
        // 	btn.OnOver += HandleDown;
        // 	btn.OnOut += HandleUp;
        // }

        m_curLvl = PlayerPrefs.GetInt("Unlocked_Level", 1);

		for(int i = 1; i< environmentButtons.Length+1; i++){
			//if(i>m_curLvl){
			//	environmentButtons[i-1].OnOver += showLocked;
			//}else{
				//environmentButtons [i-1].OnDone += OnChooseEnvironment1;
				environmentButtons[i-1].OnOver += HandleDown;
				environmentButtons[i-1].OnOut += HandleUp;
			//}
		}


		environmentButtons [0].OnDone += OnChooseEnvironment1;
		environmentButtons [1].OnDone += OnChooseEnvironment2;
		environmentButtons [2].OnDone += OnChooseEnvironment3;
		environmentButtons [3].OnDone += OnChooseEnvironment4;
		environmentButtons [4].OnDone += OnChooseEnvironment5;
    }

	/// <summary>
	/// Unregister all the event here.
	/// </summary>
	void OnDisable () 
	{
		foreach(VRInteractiveItem btn in environmentButtons)
		{
			btn.OnOver -= HandleDown;
			btn.OnOut -= HandleUp;

		}
	}


	/// <summary>
	/// On hover over Button
	/// </summary>
	private void HandleDown ()
	{
		print ("on hover");
		Camera.main.GetComponent<VREyeRaycaster> ().m_CurrentInteractible.sliderImg.value += 0.1f;
		StartCoroutine( startLoading (Camera.main.GetComponent<VREyeRaycaster> ().m_CurrentInteractible.sliderImg));
	}

	private void showLocked ()
	{
		Camera.main.GetComponent<VREyeRaycaster> ().m_CurrentInteractible.GetComponent<Image>().sprite = Camera.main.GetComponent<VREyeRaycaster> ().m_CurrentInteractible.lockImg;
		StopAllCoroutines();
	}

	/// <summary>
	/// On hover out Button
	/// </summary>
	private void HandleUp ()
	{
		print ("on hover out");
		Camera.main.GetComponent<VREyeRaycaster> ().m_LastInteractible.sliderImg.value = 0;
		StopAllCoroutines ();
	}

	IEnumerator startLoading(Slider sliderImg)
	{
		while(sliderImg.value < 1)
		{
			
			yield return new WaitForSeconds (0.05f);
			sliderImg.value += 0.1f;
		}
		Camera.main.GetComponent<VREyeRaycaster> ().m_CurrentInteractible.Done ();

	}

	/// <summary>
	/// On choose environment 1
	/// </summary>
	public void OnChooseEnvironment1()
	{
		
		OnLevelChosed (1);
	}

	/// <summary>
	/// On choose environment 2
	/// </summary>
	public void OnChooseEnvironment2()
	{
		OnLevelChosed (1); // Playing exact same environments for now
	}

	/// <summary>
	/// On choose environment 3
	/// </summary>
	public void OnChooseEnvironment3()
	{
		OnLevelChosed (1);
	}

    public void OnChooseEnvironment4()
    {
        OnLevelChosed(1);
    }

    public void OnChooseEnvironment5()
    {
        OnLevelChosed(1);
    }

    /// <summary>
    /// Raises the level chosed event. Change scene sccording to user choice.
    /// </summary>
    /// <param name="level">Level.</param>
    void OnLevelChosed(int level)
	{
		PlayerPrefs.SetInt ("level",level);
		print ("level chosed "+level);
		gameObject.SetActive (false);
        //videoScreen.SetActive (true);
        uiHandler.DisplayInstructionUI();
	}

}
