﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class SceneSetupScript : MonoBehaviour {

	public static int taskLevel = 0;
	public int timesTried;
	public int timesFailed;
	public int tasksCompleted;
	public int candidate;
	public int blueprint;
	public string nameSlug = "";
	public string sessionKey = "";
	public bool hasApplied = true;
	public bool hasFailed;
	public bool hasCompletedSimulation;
	public bool isAndroidLaunch = true;
	#if UNITY_ANDROID && !UNITY_EDITOR
	void Start () {
//		taskLevel = 1;
		if (isAndroidLaunch) {
			
			AndroidJavaClass UnityPlayer = new AndroidJavaClass ("com.unity3d.player.UnityPlayer"); 
			AndroidJavaObject currentActivity = UnityPlayer.GetStatic<AndroidJavaObject> ("currentActivity");
			AndroidJavaObject intent = currentActivity.Call<AndroidJavaObject> ("getIntent");
			AndroidJavaObject extras = intent.Call<AndroidJavaObject> ("getExtras");
			sessionKey = extras.Call<string> ("getString", "sessionKey");
			tasksCompleted = extras.Call<int> ("getInt", "taskLevel");
			timesTried = extras.Call<int> ("getInt", "timesTried");
			timesFailed = extras.Call<int> ("getInt", "timesFailed");
		} else {
			tasksCompleted = 1;
		}

		taskLevel = tasksCompleted;
//		taskLevel = 1;
		if (taskLevel > 0) {
			ChangeScene (taskLevel);
		}
	}

	// Check current scene and change level
	public void ChangeScene (int level) {
			SceneManager.LoadScene (level);
	}
	#endif
}