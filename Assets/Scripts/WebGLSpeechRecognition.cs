﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using UnityWebGLSpeechDetection;

public class WebGLSpeechRecognition : MonoBehaviour
{
    public static WebGLSpeechRecognition Instance;
    bool init = false; //Probably init on interaction to avoid AudioContext error 
    bool isRecording = false;
    bool pendingResult = false;
    bool ignoreResult = false;
    bool ignoreResult2 = false;  // Keep Check for 1 sec if extraRecordings are finalized 
    float speechDelay = 0;
    bool waiting = false;
    bool waiting2 = false; // Not recieving any new speech but wait for current timer to settle

    [SerializeField] string finalSpeech = "";

    List<string> extraRecordings = new List<string>();
    List<string> finalRecordings = new List<string>();


    ///// <summary>
    ///// Reference to the text that displays detected words
    ///// </summary>
    //public Text _mTextDictation = null;

    ///// <summary>
    ///// Reference to the example text summary
    ///// </summary>
    //public Text _mTextSummary = null;

    ///// <summary>
    ///// Reference to a warning if plugin is not available
    ///// </summary>
    //public Text _mTextWarning = null;

    ///// <summary>
    ///// Dropdown selector for languages
    ///// </summary>
    //public Dropdown _mDropDownLanguages = null;

    ///// <summary>
    ///// Dropdown selector for dialects
    ///// </summary>
    //public Dropdown _mDropDownDialects = null;

    public event Action<string> SpeechRecognizedSuccessEvent;

    /// <summary>
    /// Reference to the plugin
    /// </summary>
    private ISpeechDetectionPlugin _mSpeechDetectionPlugin = null;

    /// <summary>
    /// Reference to the supported languages and dialects
    /// </summary>
    private LanguageResult _mLanguageResult = null;

    /// <summary>
    /// List of detected words
    /// </summary>
    private List<string> _mWords = new List<string>();

    /// <summary>
    /// String builder to format the dictation text
    /// </summary>
    //private StringBuilder _mStringBuilder = new StringBuilder();

    /// <summary>
    /// Set the starting UI layout
    /// </summary>
    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    // Use this for initialization
    IEnumerator Start()
    {
        print("init Speech");
        // get the singleton instance
#if UNITY_EDITOR
        new GameObject("ProxySpeechDetectionPlugin").AddComponent<ProxySpeechDetectionPlugin>();
        _mSpeechDetectionPlugin = ProxySpeechDetectionPlugin.GetInstance();
#else
        _mSpeechDetectionPlugin = WebGLSpeechDetectionPlugin.GetInstance();
#endif
        // check the reference to the plugin
        if (null == _mSpeechDetectionPlugin)
        {
            Debug.LogError("WebGL Speech Detection Plugin is not set!");
            yield break;
        }

        // wait for plugin to become available
        while (!_mSpeechDetectionPlugin.IsAvailable())
        {
            yield return null;
        }
        _mSpeechDetectionPlugin.SetLanguage("English");
        // subscribe to events
        _mSpeechDetectionPlugin.AddListenerOnDetectionResult(HandleDetectionResult);
        StopRecord();
        // Get languages from plugin,
        //_mSpeechDetectionPlugin.GetLanguages((languageResult) =>
        //{
        //    _mLanguageResult = languageResult;



        //        // set the default language
        //    //SpeechDetectionUtils.RestoreLanguage(SpeechDetectionUtils.GetLanguage(languageResult,"English"));

        //    // set the default dialect
        //    SpeechDetectionUtils.RestoreDialect(_mDropDownDialects);

        //    //// prepare the language drop down items
        //    //SpeechDetectionUtils.PopulateLanguagesDropdown(_mDropDownLanguages, _mLanguageResult);

        //    //// subscribe to language change events
        //    //if (_mDropDownLanguages)
        //    //{
        //    //    _mDropDownLanguages.onValueChanged.AddListener(delegate {
        //    //        SpeechDetectionUtils.HandleLanguageChanged(_mDropDownLanguages,
        //    //            _mDropDownDialects,
        //    //            _mLanguageResult,
        //    //            _mSpeechDetectionPlugin);
        //    //    });
        //    //}

        //    //// subscribe to dialect change events
        //    //if (_mDropDownDialects)
        //    //{
        //    //    _mDropDownDialects.onValueChanged.AddListener(delegate {
        //    //        SpeechDetectionUtils.HandleDialectChanged(_mDropDownDialects,
        //    //            _mLanguageResult,
        //    //            _mSpeechDetectionPlugin);
        //    //    });
        //    //}

        //    // Disabled until a language is selected
        //    //SpeechDetectionUtils.DisableDialects(_mDropDownDialects);

                
        //});
    }

    private void Update()
    {
        if (speechDelay > 0)
        {
            speechDelay -= Time.deltaTime;
            print("--Waiting--");
        }
        if (waiting2 && speechDelay <= 0)
        {
            waiting2 = false;
            StopRecord();
            print("Speech Final:::" + finalSpeech);
            AppController.instance.speech.text = finalSpeech;
            if (SpeechRecognizedSuccessEvent != null)
            {
                SpeechRecognizedSuccessEvent(finalSpeech);
            }
        }
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    /// <summary>
    /// Handler for speech detection events
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    bool HandleDetectionResult(DetectionResult detectionResult)
    {
        print("Detect Speech:");
        SpeechRecognitionResult[] results = detectionResult?.results;
        if (results == null)
        {
            return false;
        }
        foreach (SpeechRecognitionResult result in results)
        {
            SpeechRecognitionAlternative[] alternatives = result.alternatives;
            if (null == alternatives)
            {
                continue;
            }
            foreach (SpeechRecognitionAlternative alternative in alternatives)
            {
                if (string.IsNullOrEmpty(alternative.transcript))
                {
                    continue;
                }
                else if (!isRecording && !result.isFinal)
                {
                    pendingResult = true;
                    //print("/// Extra Recording: " + alternative.transcript);
                    extraRecordings.Add(alternative.transcript);
                }
                if (isRecording && waiting)
                {
                    speechDelay = AppController.instance.speechDelay; // Reset on more speech
//                    waiting2 = false;
                }

                if (AppController.instance.micOn.activeSelf)
                    AppController.instance.speech.text = alternative.transcript;

                if (result.isFinal)
                {
                    if (!isRecording)
                    {
                        pendingResult = false;
                        finalRecordings.Add(alternative.transcript);
                    }
                    if (ignoreResult)
                    {
                        print("/// Droped Recording: " + alternative.transcript);
                        pendingResult = false;
                        ignoreResult = false;
                        ignoreResult2 = true;
                        AppController.instance.speech.text = "";
                        StopAllCoroutines();
                        StartCoroutine(TimerCheck());
                        AppController.instance.Mic(true);
                        return false;
                    }
                    else if (isRecording)
                    {
                        if (ignoreResult2)
                        {
                            int count = 0;
                            for (int i = extraRecordings.Count-1; i >= (extraRecordings.Count<8?0: extraRecordings.Count-7); i--)
                            {
                                var words = extraRecordings[i].ToLower().Split(' ');
                                foreach (var w in words)
                                    if (alternative.transcript.ToLower().Contains(w)) count++;
                            }
                            var wordss = AppController.instance.videoText.ToLower().Split(' ');
                            foreach (var w in wordss)
                                if (alternative.transcript.ToLower().Contains(w)) count++;
                            print("\\\\ Drop count: " + count);
                            if (count>1)
                            {
                                AppController.instance.speech.text = "";
                                print("/// Droped 2nd Recording: " + alternative.transcript);
                                return false;
                                //ignoreResult2 = false;

                            }
                        }

                        //if (waiting && speechDelay <= 0)
                        //{
                        //    StopRecord();
                        //    print("Speech Final:::" + finalSpeech);
                        //    AppController.instance.speech.text = finalSpeech;
                        //    if (SpeechRecognizedSuccessEvent != null)
                        //    {
                        //        SpeechRecognizedSuccessEvent(finalSpeech);
                        //    }
                        //}
                        //else
                        if (!waiting)
                        {
                            waiting = true;
                            speechDelay = AppController.instance.speechDelay;
                            waiting2 = true;
                            finalSpeech = alternative.transcript;
                        }
                        else
                        {
                            if (finalSpeech == "")
                                finalSpeech = alternative.transcript;
                            else if (!finalSpeech.Contains(alternative.transcript))
                            {
                                finalSpeech += " " + alternative.transcript;
                                // Found new Speech Renew pause
                                //speechDelay = AppController.instance.speechDelay;
                            }

                            if (speechDelay <= 0)
                            {
                                StopRecord();
                                print("Speech Final:::" + finalSpeech);
                                AppController.instance.speech.text = finalSpeech;
                                SpeechRecognizedSuccessEvent?.Invoke(finalSpeech);
                            }
                            else
                                waiting2 = true;
                        }
                    }
                }
            }
        }
        
        return false;
    }
    private IEnumerator TimerCheck()

    {
        yield return new WaitForSeconds(2);
        ignoreResult2 = false;
    }

    public void StartRecord()
    {
        print("********Start Speech Record");
        isRecording = true;
        finalSpeech = "";
        waiting = waiting2 = false;
        if (pendingResult)
        {
            print("/// Ignore Next Recording");
            ignoreResult = true;
        }
        else
        {
            ignoreResult2 = true; //Keep watch
            StopAllCoroutines();
            StartCoroutine(TimerCheck());
            AppController.instance.Mic(true);
        }
        //if (_mSpeechDetectionPlugin is WebGLSpeechDetectionPlugin && !_mSpeechDetectionPlugin.IsAvailable())
        //(_mSpeechDetectionPlugin as WebGLSpeechDetectionPlugin).StartRecognition();
    }

    public void StopRecord()
    {
        print("***Stop Speech Record");
        AppController.instance.Mic(false);
        waiting = false;
        extraRecordings.Clear();
        finalRecordings.Clear();
        isRecording = false;
        //if (_mSpeechDetectionPlugin is WebGLSpeechDetectionPlugin)
        //    (_mSpeechDetectionPlugin as WebGLSpeechDetectionPlugin).StopRecognition();
        //print("***Check Speech :"+ _mSpeechDetectionPlugin.IsAvailable());

    }
}
 