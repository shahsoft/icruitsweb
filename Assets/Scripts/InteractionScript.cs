﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRStandardAssets.Utils;
using System;
using SimpleJSON;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class InteractionScript : MonoBehaviour {

	[SerializeField] private SelectionSlider interactionSlider;
	[SerializeField] private SelectionSlider appQuitSlider;
	[SerializeField] private SelectionSlider scenarioYesSlider;
	[SerializeField] private SelectionSlider scenarioNoSlider;
	[SerializeField] private SelectionSlider scenarioContinueSlider;
//	[SerializeField] private SelectionSlider startSecuritySlider;
//	[SerializeField] private SelectionSlider stopSecuritySlider;
	public CanvasGroup instructions;
	public Transform maincameraPos;
	public Transform doorTransform;
	public AudioSource doorSound;

	public event Action onSecuirtyClipRecorded;

	public AudioClip securityClip;
	private string _microphoneDevice;

	public Vector3 cameraStartPos = new Vector3 (6.3f, 0f, -7.0f);
	public int taskLevel;
	public int NoVRtaskLevel;
	public float firstPosDisplacement = 3.5f;

	private bool onFirstMove = false;
	private bool afterDoorMove = false;
	private bool isDoorOpen = false;
	private bool onFirstRotate = false;
	private bool onSecondRotate = false;
	private bool onSecondMove = false;
	private bool onSit = false;

	private bool onEnd = false;
	public bool isInterationCompleted = false;

	private Vector3 startPos;
	private Vector3 endFirstPos;
	private Vector3 endPos;
	private Vector3 afterDoorPos;
	private float startTime;

	private int rotSpeed;

	public UIHandler uiHandler;

	private Quaternion camRotation;
	private Quaternion doorTempRot;
	public bool isMettingRoom;

	// Use this for initialization
	void Start () 
	{   
		interactionSlider.OnBarFilled += StartUserInteraction;
		appQuitSlider.OnBarFilled2 += EndUserInteraction;
		if(scenarioYesSlider) scenarioYesSlider.OnBarFilled += ScenarioYesInteraction;
		if(scenarioNoSlider) scenarioNoSlider.OnBarFilled += ScenarioNoInteraction;
		if(scenarioContinueSlider) scenarioContinueSlider.OnBarFilled += ScenarioContinueInteraction;
		
		int level = PlayerPrefs.GetInt ("level", 0);
		print ("change level "+level);
		// for level 2 and 3 play camera animation.
		if (level == 2 || level == 1 ) 
		{
			if (isMettingRoom) 
			{
				uiHandler.HideInstructionUI ();

			}
		} 
		else 
		{
			
	//		startSecuritySlider.OnBarFilled += StartSecurityRecord;
	//		stopSecuritySlider.OnBarFilled += StopSecuirtyRecord;
	//		uiHandler.HideSecurityUI ();
	//		StartUserInteraction ();
	//		uiHandler.HideInstructionUI ();
			// Initiale interact variables by level
			Invoke ("LevelInit", 1f);

			if (isMettingRoom) {
			
				uiHandler.HideInstructionUI ();
				//SceneManager.LoadScene (1);

				maincameraPos.position = cameraStartPos;
				startPos = maincameraPos.localPosition;
				endFirstPos = new Vector3 (startPos.x, startPos.y, startPos.z + firstPosDisplacement);
				startTime = Time.time;
				camRotation = maincameraPos.rotation;	
				onFirstMove = true;

			}
		}
	}

	void LevelInit () {
		int i = 0;
		taskLevel = SceneSetupScript.taskLevel;
		switch (taskLevel) {
		case 0:
			firstPosDisplacement = 3.5f;
			break;
		case 1:
			firstPosDisplacement = 17.5f;
			break;
		default:
            firstPosDisplacement = 5.5f;
            break;
		}

		if (!uiHandler.isVR) {
			ShowDemo ();
		}
	}

	void OnDestory () {
		interactionSlider.OnBarFilled -= StartUserInteraction;
		appQuitSlider.OnBarFilled2 -= EndUserInteraction;
		scenarioYesSlider.OnBarFilled -= ScenarioYesInteraction;
		scenarioNoSlider.OnBarFilled -= ScenarioNoInteraction;
		scenarioContinueSlider.OnBarFilled -= ScenarioContinueInteraction;
//		startSecuritySlider.OnBarFilled -= StartSecurityRecord;
//		stopSecuritySlider.OnBarFilled -= StopSecuirtyRecord;
	}

	public void StartInteractionNoVr () {
//		taskLevel = NoVRtaskLevel;
//		taskLevel = 1;
		StartUserInteraction ();
	}

	void StartUserInteraction() 
	{
        uiHandler.HideStartInteractionButton();//HideInstructionUI ();

        if (isMettingRoom && SceneSetupScript.taskLevel==1)
        {
            SceneSetupScript.taskLevel = 4;
            Debug.Log("change level Factory");
            // 
            AppController.instance.conversationState = "";
            Camera.main.GetComponent<VoiceHandler>().StartCoroutine(Camera.main.GetComponent<VoiceHandler>().GetFileNameFromConvApi("Initialization Room 2"));
            //SceneManager.LoadScene("Factory");
        }
        else
        {
            int level = PlayerPrefs.GetInt ("level", 1);
            SceneSetupScript.taskLevel = level;
            //Unlock next Level
            PlayerPrefs.SetInt("Unlocked_Level", level + 1);

            Debug.Log ("change level "+level);
            AppController.instance.conversationState = "";
            Camera.main.GetComponent<VoiceHandler>().StartCoroutine(Camera.main.GetComponent<VoiceHandler>().GetFileNameFromConvApi("Initialization Room 1"));
            //SceneManager.LoadScene (level);//
        }

//		maincameraPos.position = cameraStartPos;
//		startPos = maincameraPos.localPosition;
//		endFirstPos = new Vector3 (startPos.x, startPos.y, startPos.z + firstPosDisplacement);
//		startTime = Time.time;
//		camRotation = maincameraPos.rotation;	
//		onFirstMove = true;
	}

	void ScenarioYesInteraction()
	{
		StartCoroutine(GetUpdate(true));
	}

	void ScenarioNoInteraction()
	{
		StartCoroutine(GetUpdate(false));
	}
	void ScenarioContinueInteraction()
	{
		WebGLSpeechRecognition.Instance.StartRecord();
		uiHandler.HideScenarioUI();
		uiHandler.ShowSecurityUI(); 
	}
	
	IEnumerator GetUpdate(bool satisfy)
	{
		scenarioNoSlider.gameObject.SetActive(false);
		scenarioYesSlider.gameObject.SetActive(false);
		
		WWWForm form = new WWWForm();
		form.AddField ("scenariosuccess", satisfy.ToString());
		form.AddField ("passcode", AppController.instance.savedPass);
		UnityWebRequest www = UnityWebRequest.Post("https://icruitsdemo.mybluemix.net/icruits/update", form);
		yield return www.SendWebRequest();
		if (www.isNetworkError) {
			Debug.Log ("update error");
			Debug.Log (www.error);
			WebGLSpeechRecognition.Instance.StartRecord();
			uiHandler.HideScenarioUI();
			uiHandler.ShowSecurityUI();
		}
		else
		{
			var jsn = JSON.Parse(www.downloadHandler.text);
			Debug.Log("Update Resp: " + jsn["displayMessage"]);
			WebGLSpeechRecognition.Instance.StartRecord();
			uiHandler.HideScenarioUI();
			uiHandler.ShowSecurityUI();
//			scenarioContinueSlider.gameObject.SetActive(true);
//			uiHandler.UpdateScenarioUI(jsn["displayMessage"]);
		}
	}
	
	public void EndUserInteraction (bool alt=false) {
        print("--------END-----------   "+ SceneSetupScript.taskLevel);
        if (SceneSetupScript.taskLevel == 4 && alt) AppController.instance.returnFromScenario = true;
        if (!isMettingRoom)
        {
            uiHandler.HideInstructionUI();
            uiHandler.environmentScreen.gameObject.SetActive(false);
            uiHandler.HideSecurityUI();
            appQuitSlider.gameObject.SetActive(false);
            uiHandler.VideoScreen.SetActive(false);
            uiHandler.quitScreen.SetActive(true); //Disable Uploading
            //Application.Quit(); 
            //System.Diagnostics.Process.GetCurrentProcess().Kill();// Alternative Quit
        }
        else if (SceneSetupScript.taskLevel==1 && alt)  //Get Ticket for Factory
        {
            print("Get Factory Task Ticket");
            appQuitSlider.gameObject.SetActive(false);
            uiHandler.DisplayInstructionUI();
        }
        else
        {
		    onEnd = true;

		    Invoke ("QuitApplication", 0.1f);
        }
	}

	void QuitApplication () 
	{
		SceneManager.LoadScene("waitingArea 1");
		//Application.Quit ();
	}

	void MoveCameraPos () {
		maincameraPos.position += transform.forward * Time.deltaTime * 1;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (taskLevel.Equals (0)) {
			if (onFirstMove) {
				maincameraPos.position = Vector3.Lerp (startPos, endFirstPos, (Time.time - startTime) * 0.3f);
				if (endFirstPos.z.Equals (maincameraPos.position.z)) {
					onFirstMove = false;
					isDoorOpen = true;

				}
			}

			if (isDoorOpen) {
				//			doorTempRot *= Quaternion.AngleAxis (-90.0f, Vector3.up);
				doorTransform.RotateAround (Vector3.zero, Vector3.forward, -90.0f);
				startTime = Time.time;
				//doorSound.Play();
				isDoorOpen = false;
				afterDoorMove = true;
			}

			if (afterDoorMove) {
				//			Vector3 afterDoorStartPos = maincameraPos.localPosition;
				afterDoorPos = new Vector3 (endFirstPos.x, endFirstPos.y, endFirstPos.z + 2.5f);
				maincameraPos.position = Vector3.Lerp (endFirstPos, afterDoorPos, (Time.time - startTime) * 0.3f);
				if (afterDoorPos.Equals (maincameraPos.position)) {
					afterDoorMove = false;
					onFirstRotate = true;
				}
			}

			if (onFirstRotate) {
				camRotation *= Quaternion.AngleAxis (-73.0f, Vector3.up);
				rotSpeed = 10;
				onFirstRotate = false;
				endPos = new Vector3 (afterDoorPos.x - 4.8f, afterDoorPos.y, afterDoorPos.z + 2.3f);
				startTime = Time.time;
				onSecondMove = true;
			}

			//		doorTransform.rotation = Quaternion.Lerp (doorTransform.rotation, doorTempRot, 10 * Time.deltaTime);
			maincameraPos.rotation = Quaternion.Lerp (maincameraPos.rotation, camRotation, rotSpeed * Time.deltaTime);


			if (onSecondMove) {
				maincameraPos.position = Vector3.Lerp (afterDoorPos, endPos, (Time.time - startTime) * 0.3f);
				if (endPos.Equals (maincameraPos.position)) {
					onSecondMove = false;
					onSecondRotate = true;
				}
			}

			if (onSecondRotate) {
				//			maincameraPos.RotateAround (Vector3.zero, Vector3.up, -17.0f);
				//			camRotation = maincameraPos.rotation;
				camRotation *= Quaternion.AngleAxis (-17.0f, Vector3.up);
				rotSpeed = 4;
				onSecondRotate = false;
				onSit = true;
			}

			if (onSit) {
				Vector3 tempPos = maincameraPos.localPosition;
				maincameraPos.position = new Vector3 (tempPos.x, tempPos.y -0.5f, tempPos.z);
				onSit = false;
				isInterationCompleted = true;
			}

			if (onEnd) {
				camRotation *= Quaternion.AngleAxis (-90.0f, Vector3.up);
				rotSpeed = 1;
				onEnd = false;
				uiHandler.DisplayEndScreen ();
			}
		} 
		else if (taskLevel.Equals (1)) {
			if (onFirstMove) {
				maincameraPos.position = Vector3.Lerp (startPos, endFirstPos, (Time.time - startTime) * 0.1f);
				if (endFirstPos.z.Equals (maincameraPos.position.z)) {
					onFirstMove = false;
//					isDoorOpen = true;
					onFirstRotate = true;
				}
			}

//			if (isDoorOpen) {
//				//			doorTempRot *= Quaternion.AngleAxis (-90.0f, Vector3.up);
//				doorTransform.RotateAround (Vector3.zero, Vector3.forward, -90.0f);
//				startTime = Time.time;
//				doorSound.Play();
//				isDoorOpen = false;
//				afterDoorMove = true;
//			}

//			if (afterDoorMove) {	
//				//			Vector3 afterDoorStartPos = maincameraPos.localPosition;
//				afterDoorPos = new Vector3 (endFirstPos.x, endFirstPos.y, endFirstPos.z + 2.5f);
//				maincameraPos.position = Vector3.Lerp (endFirstPos, afterDoorPos, (Time.time - startTime) * 0.3f);
//				if (afterDoorPos.Equals (maincameraPos.position)) {
//					afterDoorMove = false;
//					onFirstRotate = true;
//				}
//			}

			if (onFirstRotate) {
				camRotation *= Quaternion.AngleAxis (-90.0f, Vector3.up);
				rotSpeed = 10;
				onFirstRotate = false;
				endPos = new Vector3 (endFirstPos.x - 0.3f, endFirstPos.y, endFirstPos.z);
				startTime = Time.time;
				onSecondMove = true;
			}

			if (onSecondMove) {
				maincameraPos.position = Vector3.Lerp (endFirstPos, endPos, (Time.time - startTime) * 0.2f);
				if (endPos.Equals (maincameraPos.position)) {
					onSecondMove = false;
					onSit = true;
				}
			}

			//		doorTransform.rotation = Quaternion.Lerp (doorTransform.rotation, doorTempRot, 10 * Time.deltaTime);
			maincameraPos.rotation = Quaternion.Lerp (maincameraPos.rotation, camRotation, rotSpeed * Time.deltaTime);

			if (onSit) {
				Vector3 tempPos = maincameraPos.localPosition;
				maincameraPos.position = new Vector3 (tempPos.x, tempPos.y -0.5f, tempPos.z);
				onSit = false;
				isInterationCompleted = true;
			}

			if (onEnd) {
				camRotation *= Quaternion.AngleAxis (-90.0f, Vector3.up);
				rotSpeed = 1;
				onEnd = false;
				uiHandler.DisplayEndScreen ();
			}
		}

	}
//
//	void StartSecurityRecord () {
////		uiHandler.HideSecurityUI ();
//		if (securityClip != null)
//			MonoBehaviour.Destroy(securityClip);
//		securityClip = Microphone.Start (_microphoneDevice, true, 10, 16000);
//	}
//
//	void StopSecuirtyRecord () {
//		Microphone.End(_microphoneDevice);
////		uiHandler.HideSecurityUI ();
//		onSecuirtyClipRecorded ();
//	}

	public void ShowDemo () {
		uiHandler.HideSecurityUI ();
        // Show Intro Video if 1st run otherwise Show Environments
        if (AppController.instance.IsFirstrun && AppController.instance.authInfo.playintro)
        {
            uiHandler.VideoScreen.SetActive(true);
            uiHandler.introVideo.OnVideoEnded += IntroVideoEnded;
        }
        else
		    uiHandler.DisplayEnvironmentUI ();
	}

    private void IntroVideoEnded()
    {
        uiHandler.introVideo.OnVideoEnded -= IntroVideoEnded;
        Invoke("ReallyIntroEnded", 2);
    }

    private void ReallyIntroEnded()
    {
        uiHandler.DisplayEnvironmentUI();
    }

    internal void PauseUserInteraction()
    {
        uiHandler.DisplayPauseUI();
    }

    //	private void CheckMicrophones()
    //	{
    //		if (Microphone.devices.Length > 0)
    //		{
    //			_microphoneDevice = Microphone.devices[0];
    ////			IsCanWork = true;
    //		}
    //		else
    //		{
    //			Debug.Log("Microphone devices not found!");
    ////			IsCanWork = false;
    //		}
    //	}
}
