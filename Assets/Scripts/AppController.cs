﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AppController : MonoBehaviour
{
    public static AppController instance;

    bool _firstrun = true;
    public List<MyMats> mats;
    public string[] selectActors;
    int _currentlevel = 0;
    public bool returnFromScenario; 

    public AuthInfo authInfo;
    public string conversationState = "";
    public string conversationScene = "";
    public string conversationWaitMsg = "";
    public int conversationWaitTimer = 0;

    public Texture2D ticketTexture;
    public Sprite ticketSprite;

    public float speechDelay;
    public Text speech;
    public GameObject micOn;
    public GameObject micOff;
    public string videoText;
    public GameObject canvas;

    public List<RenderTexture> renderTextures; // Assign RT to Video Mats on Scene Setup
    public string savedPass;

    //public enum AppState { Menu , Environment1 , Environment2, Environment3, Uploading };
    //public AppState state=AppState.Menu;

    public bool IsFirstrun
    {
        get
        {
            if (_firstrun)
            {
                _firstrun = false;
                return true;
            }
            return _firstrun;
        }
    }

    public int CurrentLevel { get => _currentlevel; set => _currentlevel = value; }

    //public GameObject introVideo;
    //public UiHandler ui;

    private void Awake()
    {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(this);
            SavWav.ClearFiles();
            PlayerPrefs.SetInt("level", 0);
            //Cursor.visible = false;

//#if UNITY_WEBGL && !UNITY_EDITOR
//            Microphone.Init();
//            Microphone.QueryAudioInput();
//#endif
        }
        else if (instance!=this)
        {
            Destroy(this);
        }
    }

    public Material GetMaterial(string actor)
    {
        foreach(var pair in mats)
            if (pair.key.Equals(actor.Substring(0, 2)))
                return pair.material;
        return null;
    }

    public RenderTexture GetRenderTexture(int actor)
    {
        if (renderTextures.Count > actor)
            return renderTextures[actor];
        RenderTexture rt = new RenderTexture(1280, 720, 24, RenderTextureFormat.ARGB32);
        rt.Create();
        renderTextures.Add(rt);
        return rt;
    }

    // Update is called once per frame
    void Update()
    {
//#if UNITY_WEBGL && !UNITY_EDITOR
//            Microphone.Update();
//#endif
    }

    public void DownloadTicketImage(bool lobby=false)
    {
        //USername_JOb_Environment_TimeStamp <--- Save Wave
        ticketSprite = null;
        var str = authInfo.environment[1].scenarios[CurrentLevel].instructionfilepath.Split(new char[] { '?' })[0];
        if (lobby) str = authInfo.tryoutinstructionfilepath;
        StartCoroutine(MakeRequest(str));
    }

    public void SetActors(string str)
    {
        string[] clipNames = str.Split(';');

        if (renderTextures.Count > 0)
            foreach (var rt in renderTextures) rt.Release();

        selectActors = new string[clipNames.Length];

        int no = int.Parse(clipNames[0].Substring(clipNames[0].Length - 1));
        clipNames[0] = clipNames[0].Substring(0, clipNames[0].Length - 2);
        no--;
        selectActors[no] = clipNames[0];

        if (clipNames.Length > 1)
        {
            no = int.Parse(clipNames[1].Substring(clipNames[1].Length - 1));
            clipNames[1] = clipNames[1].Substring(0, clipNames[1].Length - 2);
            no--;
            selectActors[no] = clipNames[1];
        }
        if (clipNames.Length > 2)
        {
            no = int.Parse(clipNames[2].Substring(clipNames[2].Length - 1));
            clipNames[2] = clipNames[2].Substring(0, clipNames[2].Length - 2);
            no--;
            selectActors[no] = clipNames[2];
        }
        
        if (SceneSetupScript.taskLevel<4)
            SceneManager.LoadScene(SceneSetupScript.taskLevel);
        else if (SceneSetupScript.taskLevel == 4)
            SceneManager.LoadScene("Factory");
    }

    string Authenticate(string username, string password)
    {
        string auth = username + ":" +password;
        auth = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(auth));
     
        auth = "Basic " +auth;
        return auth;
    }

    IEnumerator MakeRequest(string path)
    {
        //string authorization = Authenticate("mbhan@instructions.icruits.com", "Abc123#");
        string url = path;

        UnityWebRequest www = UnityWebRequest.Get(url);
        //www.SetRequestHeader("AUTHORIZATION", authorization);

        DownloadHandlerTexture texDl = new DownloadHandlerTexture(true);
        www.downloadHandler = texDl;
        yield return www.SendWebRequest();
        if (!(www.isNetworkError || www.isHttpError))
        {
            //Texture2D t = texDl.texture;
            ticketTexture = texDl.texture;
            ticketSprite= Sprite.Create(ticketTexture, new Rect(0, 0, ticketTexture.width, ticketTexture.height),
                                      Vector2.zero, 1f);
            Camera.main.GetComponent<VoiceHandler>().uiHandler.AttachTicketImage();
        }

        if (!www.isNetworkError && !www.isHttpError)
            Debug.Log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ "+www.downloadHandler.GetType());

}

    public void Mic(bool v)
    {
        micOn.SetActive(v);
        micOff.SetActive(!v);
    }

    public void CheckScenarioProgress()
    {
        if (returnFromScenario)
        {
            returnFromScenario = false;
            Camera.main.GetComponent<VoiceHandler>().uiHandler.DisplayScenarioUI();

        }
    }
}

[System.Serializable]
public class MyMats
{
    public string key;
    public Material material;
}

[System.Serializable]
public class AuthInfo
{
    public string _id;
    public string _rev;
    public string tryoutinstructionfilepath;
    public string username;
    public int jobscenario_ID;
    public bool debug;
    public int speechpause;
    public bool playintro;
    public ScenarioObj[] environment;
    
    public string activationcode;

    public static AuthInfo CreateFromJSON(string jsonString)
    {
        if (jsonString == null) return null;
        var a = JsonUtility.FromJson<AuthInfo>(jsonString);
        var b = JSON.Parse(jsonString);
        for (int i = 0; i < a.environment.Length; i++)
        {
            var c = b["environment"][i]["scenario"].AsArray;
            a.environment[i].scenarios = new scenario[b["environment"][i]["scenario"].AsArray.Count];
            for (int j = 0; j < a.environment[i].scenarios.Length; j++)
            {
                a.environment[i].scenarios[j] = JsonUtility.FromJson<scenario>(c[j].ToString());
            }
        }
        return a;
    }

}

[System.Serializable]
public class ScenarioObj
{
    public scenario[] scenarios;
}

[System.Serializable]
public class scenario
{
    public string url;
    public string conversation_state;
    public string instructionfilepath;
    public string scene;
    public string wait_message;
    public int wait_timer;
}