﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using FrostweepGames.Plugins.GoogleCloud.TextToSpeech;

public class GCTextToSpeechController : MonoBehaviour
{
        private GCTextToSpeech _gcTextToSpeech;

        private Voice[] _voices;
        private Voice _currentVoice;

        private string[] maleVoices = { "en-US-Wavenet-D" };
        private string[] femaleVoices = { "en-US-Wavenet-E", "en-US-Wavenet-F"};
        
        public event Action FinishSpeechSynt;

        
//        public Button synthesizeButton;
//        public Button getVoicesButton;

//        public InputField contentInputFioeld;
//        public InputField pitchInputField;
//        public InputField speakingRateInputField;

//        public Toggle ssmlToggle;

//        public Dropdown languageCodesDropdown;
//        public Dropdown voiceTypesDropdown;
//        public Dropdown voicesDropdown;

        public AudioClip speechClip;

        private void Start()
        {
            _gcTextToSpeech = GCTextToSpeech.Instance;

            _gcTextToSpeech.GetVoicesSuccessEvent += _gcTextToSpeech_GetVoicesSuccessEvent; 
            _gcTextToSpeech.SynthesizeSuccessEvent += _gcTextToSpeech_SynthesizeSuccessEvent;

            _gcTextToSpeech.GetVoicesFailedEvent += _gcTextToSpeech_GetVoicesFailedEvent;
            _gcTextToSpeech.SynthesizeFailedEvent += _gcTextToSpeech_SynthesizeFailedEvent;

//            synthesizeButton.onClick.AddListener(SynthesizeButtonOnClickHandler);
//            getVoicesButton.onClick.AddListener(GetVoicesButtonOnClickHandler);

//            voicesDropdown.onValueChanged.AddListener(VoiceSelectedDropdownOnChangedHandler);
//            voiceTypesDropdown.onValueChanged.AddListener(VoiceTypeSelectedDropdownOnChangedHandler);


//            int length = Enum.GetNames(typeof(Enumerators.LanguageCode)).Length;
//            List<string> elements = new List<string>();
//
//            for(int i = 0; i < length; i++)
//                elements.Add(((Enumerators.LanguageCode)i).ToString());
//
//            languageCodesDropdown.ClearOptions();
//            languageCodesDropdown.AddOptions(elements);
//            languageCodesDropdown.value = 0;
//
//            length = Enum.GetNames(typeof(Enumerators.VoiceType)).Length;
//            elements.Clear();
//
//            for (int i = 0; i < length; i++)
//                elements.Add(((Enumerators.VoiceType)i).ToString());
//
//            voiceTypesDropdown.ClearOptions();
//            voiceTypesDropdown.AddOptions(elements);
//            voiceTypesDropdown.value = 0;

            GetVoicesButtonOnClickHandler();
        }

        public void SynthesizeSpeech(string content, int modelIndex)
        {
            AppController.instance.nextModelIndex = modelIndex;
//            string content = contentInputFioeld.text;

            if (string.IsNullOrEmpty(content) || _currentVoice == null)
                return;

            _gcTextToSpeech.Synthesize(content, new VoiceConfig()
            {
                gender = _currentVoice.ssmlGender,
                languageCode = _currentVoice.languageCodes[0],
                name = _currentVoice.name
            },
            false,
            1D,
            1D,
            _currentVoice.naturalSampleRateHertz);
        }

        private void GetVoicesButtonOnClickHandler()
        {
            _gcTextToSpeech.GetVoices(new GetVoicesRequest()
            {
                languageCode =  _gcTextToSpeech.PrepareLanguage(Enumerators.LanguageCode.en_US)
            });
        }


//        private void FillVoicesList()
//        {
//            if (_voices == null)
//                return;
//
//            List<string> elements = new List<string>();
//
//            for (int i = 0; i < _voices.Length; i++)
//            {
//                if (_voices[i].name.ToLower().Contains(((Enumerators.VoiceType)voiceTypesDropdown.value).ToString().ToLower()))
//                    elements.Add(_voices[i].name);
//            }
//
//            voicesDropdown.ClearOptions();
//            voicesDropdown.AddOptions(elements);
//
//            if (elements.Count > 0)
//            {
//                voicesDropdown.value = 0;
//                VoiceSelectedDropdownOnChangedHandler(0);
//            }
//        }

//        private void VoiceSelectedDropdownOnChangedHandler(int index)
//        {
//            var voice = _voices.ToList().Find(item => item.name.Contains(voicesDropdown.options[index].text));
//            _currentVoice = voice;
//        }

//        private void VoiceTypeSelectedDropdownOnChangedHandler(int index)
//        {
//            FillVoicesList();
//        }

        #region failed handlers

        private void _gcTextToSpeech_SynthesizeFailedEvent(string error)
        {
            Debug.Log(error);
        }

        private void _gcTextToSpeech_GetVoicesFailedEvent(string error)
        {
            Debug.Log(error);
        }

        #endregion failed handlers

        #region success handlers

        private void _gcTextToSpeech_SynthesizeSuccessEvent(PostSynthesizeResponse response)
        {
            speechClip =
                _gcTextToSpeech.GetAudioClipFromBase64(response.audioContent, Constants.DEFAULT_AUDIO_ENCODING);
//            audioSource.Play();
            FinishSpeechSynt?.Invoke();
        }

        private void _gcTextToSpeech_GetVoicesSuccessEvent(GetVoicesResponse response)
        {
            _voices = response.voices;
            _currentVoice = _voices[0]; //if find fails
            foreach (var v in _voices)
            {
                if (v.name == femaleVoices[0]) _currentVoice = v;
            }
//            FillVoicesList();
        }

        #endregion sucess handlers
    }
