﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRStandardAssets.Utils;

public class UIHandler : MonoBehaviour {
	
//	[SerializeField] private SelectionSlider interactionSlider;
//	[SerializeField] private SelectionSlider startSecuritySlider;
//	[SerializeField] private SelectionSlider stopSecuritySlider;
	public CanvasGroup interactSlider;
	public CanvasGroup instructions;
    public Transform instructionLookTarget;
    public GameObject[] instructionLevelTexts;
    public Image ticketImage;
	public CanvasGroup securityScreen;
	public CanvasGroup environmentScreen;
	public Image[] environmentButtons;
	public GameObject[] environmentButtonLocks;
    public GameObject VideoScreen;
    public GameObject quitScreen;
    public GameObject PauseScreen;
    public Text PauseText;
	public GameObject scenarioScreen;
	public Text scenarioQuestionText;
	public Text scenarioProgressText;
    
    public VideoPlayerContoller introVideo;
    public GameObject wrongPass;
	public CanvasGroup endScreen;
	public bool isVR;
	public Text interactionText;
	public Image backgroundImage;
	public Button startIneraction;
	private Vector3 sliderInitPos;

	public Sprite unLock;
	public Image loadImage;
	public Image lockImage;
	private bool isLoading = false;

	// Use this for initialization
	void Start () {
//		HideInstructionUI ();
		Invoke (nameof(HideInstructionUI), 0f);
		Invoke (nameof(HideEndScreen), 0f);
	}
	
	// Update is called once per frame
	void Update () {
		if (isLoading) {
			loadImage.transform.Rotate (0, 0, -180 * Time.deltaTime);
		}
	}

	/// <summary>
	/// Hides the instruction screen.
	/// </summary>
	public void HideInstructionUI () {
		if (isVR) {
            instructions.transform.parent.gameObject.SetActive(false);
			instructions.alpha = 0f;
			instructions.blocksRaycasts = false;
			interactSlider.alpha = 0f;
			interactSlider.blocksRaycasts = false;
			SelectionSlider slider = interactSlider.GetComponentInChildren<SelectionSlider> ();
			slider.enabled = false;
//			Vector3 temp = interactionSlider.transform.localPosition;
//			sliderInitPos = temp;
//			interactionSlider.transform.position = new Vector3 (temp.x + 500, temp.y - 500, temp.z);
		} else 
		{
			environmentScreen.gameObject.SetActive (false);
			VideoScreen.gameObject.SetActive (false);
			interactionText.gameObject.SetActive (false);
			backgroundImage.gameObject.SetActive (false);
			startIneraction.gameObject.SetActive (false);
		}
	}

    public void HideStartInteractionButton()
    {
        interactSlider.alpha = 0f;
        interactSlider.blocksRaycasts = false;
        SelectionSlider slider = interactSlider.GetComponentInChildren<SelectionSlider>();
        slider.enabled = false;
    }


	/// <summary>
	/// Hides the security screen.
	/// </summary>
	public void HideSecurityUI () {
		if (isVR) {
			securityScreen.alpha = 0f;
			securityScreen.blocksRaycasts = false;
//			Vector3 temp = securityScreen.transform.localPosition;
//			startSecuritySlider.transform.position = new Vector3 (temp.x + 500, temp.y - 500, temp.z);
//			stopSecuritySlider.transform.position = new Vector3 (temp.x + 500, temp.y - 500, temp.z);
		} else {
//			interactionText.gameObject.SetActive (false);
//			backgroundImage.gameObject.SetActive (false);
//			startIneraction.gameObject.SetActive (false);
		}
	}

	public void ShowSecurityUI () {
		if (isVR) {
			securityScreen.alpha = 1f;
			securityScreen.blocksRaycasts = true;
//			Vector3 temp = securityScreen.transform.localPosition;
//			startSecuritySlider.transform.position = new Vector3 (temp.x + 500, temp.y - 500, temp.z);
//			stopSecuritySlider.transform.position = new Vector3 (temp.x + 500, temp.y - 500, temp.z);
		} else {
//			interactionText.gameObject.SetActive (true);
//			backgroundImage.gameObject.SetActive (true);
//			startIneraction.gameObject.SetActive (true);
		}
	}
	
	public void DisplayInstructionUI () {
        //for (int i = 0; i < instructionLevelTexts.Length; i++)
        //{
        //   if (instructionLevelTexts[i]) instructionLevelTexts[i].SetActive(i == AppController.instance.CurrentLevel - 1);
        //}
        Camera.main.GetComponent<VoiceHandler>().InactivateSpeechListener();

        if (isVR) {
            instructions.transform.parent.gameObject.SetActive(true);
            instructions.alpha = 1.0f;
			instructions.blocksRaycasts = true;
            Camera.main.GetComponent<MouseLookCamera>().enabled = false;
            if (instructionLookTarget) Camera.main.transform.LookAt(instructionLookTarget);
            Camera.main.GetComponent<MouseLookCamera>().ResetRotations();
            Camera.main.GetComponent<MouseLookCamera>().enabled = true;


            //			Vector3 temp = interactionSlider.transform.localPosition;
            //			interactionSlider.transform.position = sliderInitPos;
            interactSlider.alpha = 1.0f;
			interactSlider.blocksRaycasts = true;
			SelectionSlider slider = interactSlider.GetComponentInChildren<SelectionSlider> ();
			slider.enabled = true;
		} else 
		{
			environmentScreen.gameObject.SetActive (true);
			VideoScreen.gameObject.SetActive (true);
			interactionText.gameObject.SetActive (true);
			backgroundImage.gameObject.SetActive (true);
			startIneraction.gameObject.SetActive (true);
		}
	}

    public void AttachTicketImage()
    {
        if (ticketImage != null)
        {
            ticketImage.sprite = AppController.instance.ticketSprite;
            //ticketImage.SetNativeSize(); // Do proper scroller and size
        }

    }

    public void DisplayEnvironmentUI () 
	{
		if (isVR) {
			environmentScreen.gameObject.SetActive (true);
            VideoScreen.gameObject.SetActive(false);
		} else 
		{
			
			VideoScreen.gameObject.SetActive (false);
			interactionText.gameObject.SetActive (false);
			backgroundImage.gameObject.SetActive (false);
			startIneraction.gameObject.SetActive (false);
		}
	}

	public void HideEnvironmentUI () {
		//if (isVR) 
		{
			environmentScreen.gameObject.SetActive (false);
			VideoScreen.gameObject.SetActive (true);
		} 
		//else 
		{
			
		}
	}

	public void DisplayEndScreen () {
		if (isVR) {
			endScreen.alpha = 1.0f;
			endScreen.blocksRaycasts = true;
		}
	}

	public void HideEndScreen () {
		if (isVR) {
			endScreen.alpha = 0.0f;
			endScreen.blocksRaycasts = false;
		}
	}

	public void StartSecLoading () {
		loadImage.enabled = true;
		isLoading = true;
        wrongPass.SetActive(false);
	}

	public void DisplayUnlock () {
		lockImage.sprite = unLock;
	}

	public void StopSecLoading (bool showError=false) {
		isLoading = false;
		loadImage.enabled = false;
        wrongPass.SetActive(showError);
	}

    public void SetupEnvironmentUI(int no)
    {
        for (int i = 0; i < environmentButtons.Length; i++)
        {
            if (no!=i)
            {
                environmentButtons[i].color = Color.gray;
                environmentButtonLocks[i].SetActive(true);
                environmentButtons[i].GetComponent<BoxCollider>().enabled = false;
            }
        }
    }

    public void DisplayPauseUI()
    {
        StartCoroutine(PauseUI());
    }

    IEnumerator PauseUI()
    {
        PauseText.text = AppController.instance.conversationWaitMsg;
        PauseScreen.SetActive(true);
        yield return new WaitForSeconds(AppController.instance.conversationWaitTimer);
        PauseScreen.SetActive(false);
        Camera.main.GetComponent<VoiceHandler>().WatsonSocketListener();
    }
    
    public void DisplayScenarioUI()
    {
	    WebGLSpeechRecognition.Instance.StopRecord();
	    HideSecurityUI();
	    scenarioScreen.SetActive(true);

    }

    public void HideScenarioUI()
    {
	    scenarioScreen.SetActive(false);
    }

    public void UpdateScenarioUI(string text)
    {
	    scenarioQuestionText.text = "";
	    scenarioProgressText.text = text;
	    
    }
}
