﻿using UnityEngine;

/// <summary>
/// This will rotate camera with mouse.
/// </summary>
public class MouseLookCamera : MonoBehaviour
{

    public bool Smooth;
    public bool lockCursor = true;
    public float SmoothTime = 5f;
    public float XSensitivity = 2f;
    public float YSensitivity = 2f;
   
    private bool CursorIsLocked = true;
    private Quaternion CameraTargetRotation;
	public bool IsCamPause;
   

	void Start()
    {
        CameraTargetRotation = transform.localRotation;

    }

	public void ResetCam()
	{
		IsCamPause = true;
		transform.eulerAngles = new Vector3 (0,0,0);
	}

    public void ResetRotations()
    {
        CameraTargetRotation = transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
    #if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBGL
        if (!IsCamPause) 
		{
			float yRot = Input.GetAxis("Mouse X") * XSensitivity;
			float xRot = Input.GetAxis("Mouse Y") * YSensitivity;


			CameraTargetRotation *= Quaternion.Euler(-xRot, yRot, 0f);

			float y = CameraTargetRotation.eulerAngles.y;
			float x = CameraTargetRotation.eulerAngles.x;
			/*
		if (CameraTargetRotation.eulerAngles.y >= 29.9f && CameraTargetRotation.eulerAngles.y <= 179.9f) 
		{
			y = 30;

		}
		else if (CameraTargetRotation.eulerAngles.y >= 179.9f && CameraTargetRotation.eulerAngles.y <= 329.9f) 
		{
			y = 330;
		}
		//Debug.Log(" y "+y);
		if (CameraTargetRotation.eulerAngles.x >= 50 && CameraTargetRotation.eulerAngles.x <= 180) 
		{
			x = 50;
		}
		else if (CameraTargetRotation.eulerAngles.x >= 180 && CameraTargetRotation.eulerAngles.x <= 330) 
		{
			x = 330;
		}
	//	Debug.Log(" x "+x);
*/
			CameraTargetRotation.eulerAngles = new Vector3(x, y, 0); //new Vector3(y, x, 0);//

			if (Smooth)
			{
				transform.localRotation = Quaternion.Slerp(transform.localRotation, CameraTargetRotation, SmoothTime * Time.deltaTime);
			}
			else
			{
				transform.localRotation = CameraTargetRotation;
			}

			UpdateCursorLock();
		}
		#endif
    }
    void SetCursorLock(bool value)
    {
        lockCursor = value;
        if (!lockCursor)
        {//we force unlock the cursor if the user disable the cursor locking helper
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    void UpdateCursorLock()
    {
        //if the user set "lockCursor" we check & properly lock the cursos
        if (lockCursor)
            InternalLockUpdate();
    }

    void InternalLockUpdate()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            CursorIsLocked = false;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            CursorIsLocked = true;
        }

        if (CursorIsLocked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (!CursorIsLocked)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
