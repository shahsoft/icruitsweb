﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseFocusCheck : MonoBehaviour
{
    static bool firstRun = true;
    // Start is called before the first frame update
    void Start()
    {
        if (firstRun)
        {
            firstRun = false;
        }
        else
        {
            if (Camera.main != null) Camera.main.GetComponent<MouseLookCamera>().enabled = true;
            
            // Also since its not 1st run. Let AppController handle  
            AppController.instance.CheckScenarioProgress();
            Destroy(gameObject);
        }
    }
}
