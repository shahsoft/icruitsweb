﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Video;

public class VideoPlayerContoller : MonoBehaviour {

    //public GameObject instructionScreen;
    public event Action OnVideoEnded;
	bool isVideoEnd;
	bool isVideoPlaying=false;
    int counter = 0;
	VideoPlayer mediaPlay;
    public string fileName;

	/// <summary>
	/// Play welcome video here
	/// </summary>
	void OnEnable () 
	{
		isVideoEnd  = false;
        mediaPlay = GetComponent<VideoPlayer> ();
		//mediaPlay.m_strFileName = "https://www.dropbox.com/s/a2ie4lvvi3jq5mv/IcruitsSales_cut_049.mp4?dl=1";
		//StartCoroutine (mediaPlay.DownloadStreamingVideoAndLoad2(mediaPlay.m_strFileName));
		mediaPlay.url= Application.streamingAssetsPath + Path.DirectorySeparatorChar + fileName;
	}
	
	/// <summary>
	/// Called when video ended
	/// </summary>
	IEnumerator  OnVideoEnd (float time) 
	{
		yield return new WaitForSeconds (time);
		Camera.main.GetComponent<VoiceHandler> ().WatsonSocketListener ();

	}


	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update()
	{
		if (mediaPlay.isPlaying && counter < 3) 
		{
			counter++;
            if (counter == 3)
            {
                GetComponent<MeshRenderer>().enabled = true;
                isVideoPlaying = true;
            }
		}
		else if (isVideoPlaying && !mediaPlay.isPlaying && !isVideoEnd) 
		{
			isVideoEnd = true;
            isVideoPlaying = false;
			Camera.main.GetComponent<VoiceHandler> ().WatsonSocketListener ();
            OnVideoEnded();
		} 
	}

}
