﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Video;

public class VideoModelScript : MonoBehaviour {

	private VideoPlayer mediaPlayerScript;
	private string s3Link = "https://s3-us-west-2.amazonaws.com/icruitsampletest1/";
	private bool isVideoChanged;
	public bool isTest;
    public string idleClip = "";
    public MeshRenderer renderer;

	public bool isVideoEnded = false;

	// Use this for initialization
	void Awake () {
		mediaPlayerScript = GetComponentInChildren<VideoPlayer> ();
		mediaPlayerScript.loopPointReached += OnVideoEnd;
		mediaPlayerScript.errorReceived +=  OnVideoLoadError;
		//mediaPlayerScript.m_strFileName = "idleClip.mp4"	;
		isVideoChanged = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void NextVideo() {
		ChangeVideo ();
	}

	void OnVideoEnd (VideoPlayer vp) {
        Debug.Log("Video Ended");
		isVideoEnded = true;
	}

	void OnVideoLoadError (VideoPlayer vp, string code) {
		isVideoEnded = true;
	}

	public void ChangeToVideo (string name) {
		if (isTest == true) {
			if (name.Equals ("greet_brenda")) {
				name = "clip";
			} else if (name.Equals ("greet_noname")) {
				name = "clip";
			} else {
				// Do nothing
			}
		}
        
		//if (!idleClip.Equals (""))
  //          mediaPlayerScript.Load(idleClip);
        /*else*/ if (name.Equals (""))
            {
                mediaPlayerScript.url= Application.streamingAssetsPath + Path.DirectorySeparatorChar + "idleClip.mp4";
		} else {
			mediaPlayerScript.url= Application.streamingAssetsPath + Path.DirectorySeparatorChar + name + ".mp4";
		}
	}

	/// <summary>
	/// call video call method.
	/// </summary>
	/// <param name="name">Name.</param>
	public void ChangeToVideoCode (string name) {
//		if (isTest == true) {
//			if (name.Equals ("greet_brenda")) {
//				name = "clip";
//			} else if (name.Equals ("greet_noname")) {
//				name = "clip";
//			} else {
//				// Do nothing
//			}
//		}
//
//		if (name.Equals ("")) {
//			mediaPlayerScript.Load ("idleClip.mp4");
//		} else {
//			mediaPlayerScript.Load (name + "h.mp4");
//		}

		mediaPlayerScript.url= Application.streamingAssetsPath + Path.DirectorySeparatorChar + name + ".mp4";
		print("<><><><><><><><><><><><><><><><><><><><> " + name);
		//StartCoroutine( mediaPlayerScript.DownloadStreamingVideoAndLoad2 ("https://www.dropbox.com/s/46pm6398iaofad1/IcruitsSales_cut_001.mp4?dl=1"));
//		mediaPlayerScript.Load ("IcruitsSales_cut_001" + ".mp4");
	}

	void ChangeVideo(){
//		if (videoNumber == 1) {
//		mediaPlayerScript.Stop ();
		if (!isVideoChanged) {
			mediaPlayerScript.url= Application.streamingAssetsPath + Path.DirectorySeparatorChar + "Greet_Response.mp4";
			isVideoChanged = true;
		} else {
			mediaPlayerScript.url= Application.streamingAssetsPath + Path.DirectorySeparatorChar + "Greet.mp4";
			isVideoChanged = false;
		}

//			mediaPlayerScript.m_strFileName = s3Link + "Clip_A_264.mp4";
//		mediaPlayerScript.Play ();
//			videoNumber += 1;
//		} else {
//			mediaPlayerScript.m_strFileName = s3Link + "Clip_A_264.mp4";
//			videoNumber -= 1;
//		}
	}
}
